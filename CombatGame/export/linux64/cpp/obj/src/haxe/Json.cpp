#include <hxcpp.h>

#ifndef INCLUDED_Std
#include <Std.h>
#endif
#ifndef INCLUDED_StringBuf
#include <StringBuf.h>
#endif
#ifndef INCLUDED_haxe_Json
#include <haxe/Json.h>
#endif
namespace haxe{

Void Json_obj::__construct()
{
HX_STACK_FRAME("haxe.Json","new",0x79e5c882,"haxe.Json.new","/usr/lib/haxe/std/haxe/Json.hx",39,0xae67c2ee)
{
}
;
	return null();
}

Json_obj::~Json_obj() { }

Dynamic Json_obj::__CreateEmpty() { return  new Json_obj; }
hx::ObjectPtr< Json_obj > Json_obj::__new()
{  hx::ObjectPtr< Json_obj > result = new Json_obj();
	result->__construct();
	return result;}

Dynamic Json_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Json_obj > result = new Json_obj();
	result->__construct();
	return result;}

Dynamic Json_obj::doParse( ::String str){
	HX_STACK_FRAME("haxe.Json","doParse",0xfa1021ca,"haxe.Json.doParse","/usr/lib/haxe/std/haxe/Json.hx",201,0xae67c2ee)
	HX_STACK_THIS(this)
	HX_STACK_ARG(str,"str")
	HX_STACK_LINE(202)
	this->str = str;
	HX_STACK_LINE(203)
	this->pos = (int)0;
	HX_STACK_LINE(204)
	return this->parseRec();
}


HX_DEFINE_DYNAMIC_FUNC1(Json_obj,doParse,return )

Void Json_obj::invalidChar( ){
{
		HX_STACK_FRAME("haxe.Json","invalidChar",0x7e28978f,"haxe.Json.invalidChar","/usr/lib/haxe/std/haxe/Json.hx",207,0xae67c2ee)
		HX_STACK_THIS(this)
		HX_STACK_LINE(208)
		(this->pos)--;
		HX_STACK_LINE(209)
		HX_STACK_DO_THROW((((HX_CSTRING("Invalid char ") + this->str.cca(this->pos)) + HX_CSTRING(" at position ")) + this->pos));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Json_obj,invalidChar,(void))

Dynamic Json_obj::parseRec( ){
	HX_STACK_FRAME("haxe.Json","parseRec",0x28e62f7b,"haxe.Json.parseRec","/usr/lib/haxe/std/haxe/Json.hx",217,0xae67c2ee)
	HX_STACK_THIS(this)
	HX_STACK_LINE(217)
	while((true)){
		HX_STACK_LINE(218)
		int c;		HX_STACK_VAR(c,"c");
		HX_STACK_LINE(218)
		{
			HX_STACK_LINE(218)
			int index = (this->pos)++;		HX_STACK_VAR(index,"index");
			HX_STACK_LINE(218)
			c = this->str.cca(index);
		}
		HX_STACK_LINE(219)
		switch( (int)(c)){
			case (int)32: case (int)13: case (int)10: case (int)9: {
			}
			;break;
			case (int)123: {
				struct _Function_3_1{
					inline static Dynamic Block( ){
						HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","/usr/lib/haxe/std/haxe/Json.hx",223,0xae67c2ee)
						{
							hx::Anon __result = hx::Anon_obj::Create();
							return __result;
						}
						return null();
					}
				};
				HX_STACK_LINE(223)
				Dynamic obj = _Function_3_1::Block();		HX_STACK_VAR(obj,"obj");
				HX_STACK_LINE(223)
				::String field = null();		HX_STACK_VAR(field,"field");
				HX_STACK_LINE(223)
				Dynamic comma = null();		HX_STACK_VAR(comma,"comma");
				HX_STACK_LINE(224)
				while((true)){
					HX_STACK_LINE(225)
					int c1;		HX_STACK_VAR(c1,"c1");
					HX_STACK_LINE(225)
					{
						HX_STACK_LINE(225)
						int index = (this->pos)++;		HX_STACK_VAR(index,"index");
						HX_STACK_LINE(225)
						c1 = this->str.cca(index);
					}
					HX_STACK_LINE(226)
					switch( (int)(c1)){
						case (int)32: case (int)13: case (int)10: case (int)9: {
						}
						;break;
						case (int)125: {
							HX_STACK_LINE(230)
							if (((bool((field != null())) || bool((comma == false))))){
								HX_STACK_LINE(231)
								this->invalidChar();
							}
							HX_STACK_LINE(232)
							return obj;
						}
						;break;
						case (int)58: {
							HX_STACK_LINE(234)
							if (((field == null()))){
								HX_STACK_LINE(235)
								this->invalidChar();
							}
							HX_STACK_LINE(236)
							{
								HX_STACK_LINE(236)
								Dynamic value = this->parseRec();		HX_STACK_VAR(value,"value");
								HX_STACK_LINE(236)
								if (((obj != null()))){
									HX_STACK_LINE(236)
									obj->__SetField(field,value,false);
								}
							}
							HX_STACK_LINE(237)
							field = null();
							HX_STACK_LINE(238)
							comma = true;
						}
						;break;
						case (int)44: {
							HX_STACK_LINE(240)
							if ((comma)){
								HX_STACK_LINE(240)
								comma = false;
							}
							else{
								HX_STACK_LINE(240)
								this->invalidChar();
							}
						}
						;break;
						case (int)34: {
							HX_STACK_LINE(242)
							if ((comma)){
								HX_STACK_LINE(242)
								this->invalidChar();
							}
							HX_STACK_LINE(243)
							field = this->parseString();
						}
						;break;
						default: {
							HX_STACK_LINE(245)
							this->invalidChar();
						}
					}
				}
			}
			;break;
			case (int)91: {
				HX_STACK_LINE(249)
				Dynamic arr = Dynamic( Array_obj<Dynamic>::__new());		HX_STACK_VAR(arr,"arr");
				HX_STACK_LINE(249)
				Dynamic comma = null();		HX_STACK_VAR(comma,"comma");
				HX_STACK_LINE(250)
				while((true)){
					HX_STACK_LINE(251)
					int c1;		HX_STACK_VAR(c1,"c1");
					HX_STACK_LINE(251)
					{
						HX_STACK_LINE(251)
						int index = (this->pos)++;		HX_STACK_VAR(index,"index");
						HX_STACK_LINE(251)
						c1 = this->str.cca(index);
					}
					HX_STACK_LINE(252)
					switch( (int)(c1)){
						case (int)32: case (int)13: case (int)10: case (int)9: {
						}
						;break;
						case (int)93: {
							HX_STACK_LINE(256)
							if (((comma == false))){
								HX_STACK_LINE(256)
								this->invalidChar();
							}
							HX_STACK_LINE(257)
							return arr;
						}
						;break;
						case (int)44: {
							HX_STACK_LINE(259)
							if ((comma)){
								HX_STACK_LINE(259)
								comma = false;
							}
							else{
								HX_STACK_LINE(259)
								this->invalidChar();
							}
						}
						;break;
						default: {
							HX_STACK_LINE(261)
							if ((comma)){
								HX_STACK_LINE(261)
								this->invalidChar();
							}
							HX_STACK_LINE(262)
							(this->pos)--;
							HX_STACK_LINE(263)
							arr->__Field(HX_CSTRING("push"),true)(this->parseRec());
							HX_STACK_LINE(264)
							comma = true;
						}
					}
				}
			}
			;break;
			case (int)116: {
				HX_STACK_LINE(268)
				int save = this->pos;		HX_STACK_VAR(save,"save");
				struct _Function_3_1{
					inline static int Block( ::haxe::Json_obj *__this){
						HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","/usr/lib/haxe/std/haxe/Json.hx",269,0xae67c2ee)
						{
							HX_STACK_LINE(269)
							int index = (__this->pos)++;		HX_STACK_VAR(index,"index");
							HX_STACK_LINE(269)
							return __this->str.cca(index);
						}
						return null();
					}
				};
				struct _Function_3_2{
					inline static int Block( ::haxe::Json_obj *__this){
						HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","/usr/lib/haxe/std/haxe/Json.hx",269,0xae67c2ee)
						{
							HX_STACK_LINE(269)
							int index = (__this->pos)++;		HX_STACK_VAR(index,"index");
							HX_STACK_LINE(269)
							return __this->str.cca(index);
						}
						return null();
					}
				};
				struct _Function_3_3{
					inline static int Block( ::haxe::Json_obj *__this){
						HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","/usr/lib/haxe/std/haxe/Json.hx",269,0xae67c2ee)
						{
							HX_STACK_LINE(269)
							int index = (__this->pos)++;		HX_STACK_VAR(index,"index");
							HX_STACK_LINE(269)
							return __this->str.cca(index);
						}
						return null();
					}
				};
				HX_STACK_LINE(269)
				if (((  ((!(((  ((!(((_Function_3_1::Block(this) != (int)114))))) ? bool((_Function_3_2::Block(this) != (int)117)) : bool(true) ))))) ? bool((_Function_3_3::Block(this) != (int)101)) : bool(true) ))){
					HX_STACK_LINE(270)
					this->pos = save;
					HX_STACK_LINE(271)
					this->invalidChar();
				}
				HX_STACK_LINE(273)
				return true;
			}
			;break;
			case (int)102: {
				HX_STACK_LINE(275)
				int save = this->pos;		HX_STACK_VAR(save,"save");
				struct _Function_3_1{
					inline static int Block( ::haxe::Json_obj *__this){
						HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","/usr/lib/haxe/std/haxe/Json.hx",276,0xae67c2ee)
						{
							HX_STACK_LINE(276)
							int index = (__this->pos)++;		HX_STACK_VAR(index,"index");
							HX_STACK_LINE(276)
							return __this->str.cca(index);
						}
						return null();
					}
				};
				struct _Function_3_2{
					inline static int Block( ::haxe::Json_obj *__this){
						HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","/usr/lib/haxe/std/haxe/Json.hx",276,0xae67c2ee)
						{
							HX_STACK_LINE(276)
							int index = (__this->pos)++;		HX_STACK_VAR(index,"index");
							HX_STACK_LINE(276)
							return __this->str.cca(index);
						}
						return null();
					}
				};
				struct _Function_3_3{
					inline static int Block( ::haxe::Json_obj *__this){
						HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","/usr/lib/haxe/std/haxe/Json.hx",276,0xae67c2ee)
						{
							HX_STACK_LINE(276)
							int index = (__this->pos)++;		HX_STACK_VAR(index,"index");
							HX_STACK_LINE(276)
							return __this->str.cca(index);
						}
						return null();
					}
				};
				struct _Function_3_4{
					inline static int Block( ::haxe::Json_obj *__this){
						HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","/usr/lib/haxe/std/haxe/Json.hx",276,0xae67c2ee)
						{
							HX_STACK_LINE(276)
							int index = (__this->pos)++;		HX_STACK_VAR(index,"index");
							HX_STACK_LINE(276)
							return __this->str.cca(index);
						}
						return null();
					}
				};
				HX_STACK_LINE(276)
				if (((  ((!(((  ((!(((  ((!(((_Function_3_1::Block(this) != (int)97))))) ? bool((_Function_3_2::Block(this) != (int)108)) : bool(true) ))))) ? bool((_Function_3_3::Block(this) != (int)115)) : bool(true) ))))) ? bool((_Function_3_4::Block(this) != (int)101)) : bool(true) ))){
					HX_STACK_LINE(277)
					this->pos = save;
					HX_STACK_LINE(278)
					this->invalidChar();
				}
				HX_STACK_LINE(280)
				return false;
			}
			;break;
			case (int)110: {
				HX_STACK_LINE(282)
				int save = this->pos;		HX_STACK_VAR(save,"save");
				struct _Function_3_1{
					inline static int Block( ::haxe::Json_obj *__this){
						HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","/usr/lib/haxe/std/haxe/Json.hx",283,0xae67c2ee)
						{
							HX_STACK_LINE(283)
							int index = (__this->pos)++;		HX_STACK_VAR(index,"index");
							HX_STACK_LINE(283)
							return __this->str.cca(index);
						}
						return null();
					}
				};
				struct _Function_3_2{
					inline static int Block( ::haxe::Json_obj *__this){
						HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","/usr/lib/haxe/std/haxe/Json.hx",283,0xae67c2ee)
						{
							HX_STACK_LINE(283)
							int index = (__this->pos)++;		HX_STACK_VAR(index,"index");
							HX_STACK_LINE(283)
							return __this->str.cca(index);
						}
						return null();
					}
				};
				struct _Function_3_3{
					inline static int Block( ::haxe::Json_obj *__this){
						HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","/usr/lib/haxe/std/haxe/Json.hx",283,0xae67c2ee)
						{
							HX_STACK_LINE(283)
							int index = (__this->pos)++;		HX_STACK_VAR(index,"index");
							HX_STACK_LINE(283)
							return __this->str.cca(index);
						}
						return null();
					}
				};
				HX_STACK_LINE(283)
				if (((  ((!(((  ((!(((_Function_3_1::Block(this) != (int)117))))) ? bool((_Function_3_2::Block(this) != (int)108)) : bool(true) ))))) ? bool((_Function_3_3::Block(this) != (int)108)) : bool(true) ))){
					HX_STACK_LINE(284)
					this->pos = save;
					HX_STACK_LINE(285)
					this->invalidChar();
				}
				HX_STACK_LINE(287)
				return null();
			}
			;break;
			case (int)34: {
				HX_STACK_LINE(289)
				return this->parseString();
			}
			;break;
			case (int)48: case (int)49: case (int)50: case (int)51: case (int)52: case (int)53: case (int)54: case (int)55: case (int)56: case (int)57: case (int)45: {
				HX_STACK_LINE(291)
				int c1 = c;		HX_STACK_VAR(c1,"c1");
				HX_STACK_LINE(291)
				int start = (this->pos - (int)1);		HX_STACK_VAR(start,"start");
				HX_STACK_LINE(291)
				bool minus = (c1 == (int)45);		HX_STACK_VAR(minus,"minus");
				HX_STACK_LINE(291)
				bool digit = !(minus);		HX_STACK_VAR(digit,"digit");
				HX_STACK_LINE(291)
				bool zero = (c1 == (int)48);		HX_STACK_VAR(zero,"zero");
				HX_STACK_LINE(291)
				bool point = false;		HX_STACK_VAR(point,"point");
				HX_STACK_LINE(291)
				bool e = false;		HX_STACK_VAR(e,"e");
				HX_STACK_LINE(291)
				bool pm = false;		HX_STACK_VAR(pm,"pm");
				HX_STACK_LINE(291)
				bool end = false;		HX_STACK_VAR(end,"end");
				HX_STACK_LINE(291)
				while((true)){
					HX_STACK_LINE(291)
					{
						HX_STACK_LINE(291)
						int index = (this->pos)++;		HX_STACK_VAR(index,"index");
						HX_STACK_LINE(291)
						c1 = this->str.cca(index);
					}
					HX_STACK_LINE(291)
					switch( (int)(c1)){
						case (int)48: {
							HX_STACK_LINE(291)
							if (((bool(zero) && bool(!(point))))){
								HX_STACK_LINE(291)
								this->invalidNumber(start);
							}
							HX_STACK_LINE(291)
							if ((minus)){
								HX_STACK_LINE(291)
								minus = false;
								HX_STACK_LINE(291)
								zero = true;
							}
							HX_STACK_LINE(291)
							digit = true;
						}
						;break;
						case (int)49: case (int)50: case (int)51: case (int)52: case (int)53: case (int)54: case (int)55: case (int)56: case (int)57: {
							HX_STACK_LINE(291)
							if (((bool(zero) && bool(!(point))))){
								HX_STACK_LINE(291)
								this->invalidNumber(start);
							}
							HX_STACK_LINE(291)
							if ((minus)){
								HX_STACK_LINE(291)
								minus = false;
							}
							HX_STACK_LINE(291)
							digit = true;
							HX_STACK_LINE(291)
							zero = false;
						}
						;break;
						case (int)46: {
							HX_STACK_LINE(291)
							if (((bool(minus) || bool(point)))){
								HX_STACK_LINE(291)
								this->invalidNumber(start);
							}
							HX_STACK_LINE(291)
							digit = false;
							HX_STACK_LINE(291)
							point = true;
						}
						;break;
						case (int)101: case (int)69: {
							HX_STACK_LINE(291)
							if (((bool((bool(minus) || bool(zero))) || bool(e)))){
								HX_STACK_LINE(291)
								this->invalidNumber(start);
							}
							HX_STACK_LINE(291)
							digit = false;
							HX_STACK_LINE(291)
							e = true;
						}
						;break;
						case (int)43: case (int)45: {
							HX_STACK_LINE(291)
							if (((bool(!(e)) || bool(pm)))){
								HX_STACK_LINE(291)
								this->invalidNumber(start);
							}
							HX_STACK_LINE(291)
							digit = false;
							HX_STACK_LINE(291)
							pm = true;
						}
						;break;
						default: {
							HX_STACK_LINE(291)
							if ((!(digit))){
								HX_STACK_LINE(291)
								this->invalidNumber(start);
							}
							HX_STACK_LINE(291)
							(this->pos)--;
							HX_STACK_LINE(291)
							end = true;
						}
					}
					HX_STACK_LINE(291)
					if ((end)){
						HX_STACK_LINE(291)
						break;
					}
				}
				HX_STACK_LINE(291)
				Float f = ::Std_obj::parseFloat(this->str.substr(start,(this->pos - start)));		HX_STACK_VAR(f,"f");
				HX_STACK_LINE(291)
				int i = ::Std_obj::_int(f);		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(291)
				if (((i == f))){
					HX_STACK_LINE(291)
					return i;
				}
				else{
					HX_STACK_LINE(291)
					return f;
				}
			}
			;break;
			default: {
				HX_STACK_LINE(293)
				this->invalidChar();
			}
		}
	}
	HX_STACK_LINE(217)
	return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Json_obj,parseRec,return )

::String Json_obj::parseString( ){
	HX_STACK_FRAME("haxe.Json","parseString",0x60c6bf86,"haxe.Json.parseString","/usr/lib/haxe/std/haxe/Json.hx",298,0xae67c2ee)
	HX_STACK_THIS(this)
	HX_STACK_LINE(299)
	int start = this->pos;		HX_STACK_VAR(start,"start");
	HX_STACK_LINE(300)
	::StringBuf buf = ::StringBuf_obj::__new();		HX_STACK_VAR(buf,"buf");
	HX_STACK_LINE(301)
	while((true)){
		HX_STACK_LINE(302)
		int c;		HX_STACK_VAR(c,"c");
		HX_STACK_LINE(302)
		{
			HX_STACK_LINE(302)
			int index = (this->pos)++;		HX_STACK_VAR(index,"index");
			HX_STACK_LINE(302)
			c = this->str.cca(index);
		}
		HX_STACK_LINE(303)
		if (((c == (int)34))){
			HX_STACK_LINE(304)
			break;
		}
		HX_STACK_LINE(305)
		if (((c == (int)92))){
			HX_STACK_LINE(306)
			buf->b->push(this->str.substr(start,((this->pos - start) - (int)1)));
			HX_STACK_LINE(307)
			{
				HX_STACK_LINE(307)
				int index = (this->pos)++;		HX_STACK_VAR(index,"index");
				HX_STACK_LINE(307)
				c = this->str.cca(index);
			}
			HX_STACK_LINE(308)
			switch( (int)(c)){
				case (int)114: {
					HX_STACK_LINE(309)
					buf->b->push(HX_CSTRING("\r"));
				}
				;break;
				case (int)110: {
					HX_STACK_LINE(310)
					buf->b->push(HX_CSTRING("\n"));
				}
				;break;
				case (int)116: {
					HX_STACK_LINE(311)
					buf->b->push(HX_CSTRING("\t"));
				}
				;break;
				case (int)98: {
					HX_STACK_LINE(312)
					buf->b->push(HX_CSTRING("\x08"""));
				}
				;break;
				case (int)102: {
					HX_STACK_LINE(313)
					buf->b->push(HX_CSTRING("\x0c"""));
				}
				;break;
				case (int)47: case (int)92: case (int)34: {
					HX_STACK_LINE(314)
					buf->b->push(::String::fromCharCode(c));
				}
				;break;
				case (int)117: {
					HX_STACK_LINE(316)
					Dynamic uc = ::Std_obj::parseInt((HX_CSTRING("0x") + this->str.substr(this->pos,(int)4)));		HX_STACK_VAR(uc,"uc");
					HX_STACK_LINE(317)
					hx::AddEq(this->pos,(int)4);
					HX_STACK_LINE(319)
					if (((uc <= (int)127))){
						HX_STACK_LINE(320)
						int c1 = uc;		HX_STACK_VAR(c1,"c1");
						HX_STACK_LINE(320)
						buf->b->push(::String::fromCharCode(c1));
					}
					else{
						HX_STACK_LINE(321)
						if (((uc <= (int)2047))){
							HX_STACK_LINE(322)
							buf->b->push(::String::fromCharCode((int((int)192) | int((int(uc) >> int((int)6))))));
							HX_STACK_LINE(323)
							buf->b->push(::String::fromCharCode((int((int)128) | int((int(uc) & int((int)63))))));
						}
						else{
							HX_STACK_LINE(324)
							if (((uc <= (int)65535))){
								HX_STACK_LINE(325)
								buf->b->push(::String::fromCharCode((int((int)224) | int((int(uc) >> int((int)12))))));
								HX_STACK_LINE(326)
								buf->b->push(::String::fromCharCode((int((int)128) | int((int((int(uc) >> int((int)6))) & int((int)63))))));
								HX_STACK_LINE(327)
								buf->b->push(::String::fromCharCode((int((int)128) | int((int(uc) & int((int)63))))));
							}
							else{
								HX_STACK_LINE(329)
								buf->b->push(::String::fromCharCode((int((int)240) | int((int(uc) >> int((int)18))))));
								HX_STACK_LINE(330)
								buf->b->push(::String::fromCharCode((int((int)128) | int((int((int(uc) >> int((int)12))) & int((int)63))))));
								HX_STACK_LINE(331)
								buf->b->push(::String::fromCharCode((int((int)128) | int((int((int(uc) >> int((int)6))) & int((int)63))))));
								HX_STACK_LINE(332)
								buf->b->push(::String::fromCharCode((int((int)128) | int((int(uc) & int((int)63))))));
							}
						}
					}
				}
				;break;
				default: {
					HX_STACK_LINE(338)
					HX_STACK_DO_THROW((((HX_CSTRING("Invalid escape sequence \\") + ::String::fromCharCode(c)) + HX_CSTRING(" at position ")) + ((this->pos - (int)1))));
				}
			}
			HX_STACK_LINE(340)
			start = this->pos;
		}
		else{
			HX_STACK_LINE(344)
			if (((c >= (int)128))){
				HX_STACK_LINE(345)
				(this->pos)++;
				HX_STACK_LINE(346)
				if (((c >= (int)252))){
					HX_STACK_LINE(346)
					hx::AddEq(this->pos,(int)4);
				}
				else{
					HX_STACK_LINE(347)
					if (((c >= (int)248))){
						HX_STACK_LINE(347)
						hx::AddEq(this->pos,(int)3);
					}
					else{
						HX_STACK_LINE(348)
						if (((c >= (int)240))){
							HX_STACK_LINE(348)
							hx::AddEq(this->pos,(int)2);
						}
						else{
							HX_STACK_LINE(349)
							if (((c >= (int)224))){
								HX_STACK_LINE(349)
								(this->pos)++;
							}
						}
					}
				}
			}
			else{
				HX_STACK_LINE(352)
				if (((c == (int)0))){
					HX_STACK_LINE(353)
					HX_STACK_DO_THROW(HX_CSTRING("Unclosed string"));
				}
			}
		}
	}
	HX_STACK_LINE(355)
	buf->b->push(this->str.substr(start,((this->pos - start) - (int)1)));
	HX_STACK_LINE(356)
	return buf->b->join(HX_CSTRING(""));
}


HX_DEFINE_DYNAMIC_FUNC0(Json_obj,parseString,return )

Void Json_obj::invalidNumber( int start){
{
		HX_STACK_FRAME("haxe.Json","invalidNumber",0xb52e1fc2,"haxe.Json.invalidNumber","/usr/lib/haxe/std/haxe/Json.hx",360,0xae67c2ee)
		HX_STACK_THIS(this)
		HX_STACK_ARG(start,"start")
		HX_STACK_LINE(360)
		HX_STACK_DO_THROW((((HX_CSTRING("Invalid number at position ") + start) + HX_CSTRING(": ")) + this->str.substr(start,(this->pos - start))));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(Json_obj,invalidNumber,(void))

Dynamic Json_obj::parse( ::String text){
	HX_STACK_FRAME("haxe.Json","parse",0x396f54d5,"haxe.Json.parse","/usr/lib/haxe/std/haxe/Json.hx",409,0xae67c2ee)
	HX_STACK_ARG(text,"text")
	HX_STACK_LINE(409)
	return ::haxe::Json_obj::__new()->doParse(text);
}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(Json_obj,parse,return )


Json_obj::Json_obj()
{
}

void Json_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(Json);
	HX_MARK_MEMBER_NAME(str,"str");
	HX_MARK_MEMBER_NAME(pos,"pos");
	HX_MARK_END_CLASS();
}

void Json_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(str,"str");
	HX_VISIT_MEMBER_NAME(pos,"pos");
}

Dynamic Json_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"str") ) { return str; }
		if (HX_FIELD_EQ(inName,"pos") ) { return pos; }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"parse") ) { return parse_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"doParse") ) { return doParse_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"parseRec") ) { return parseRec_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"invalidChar") ) { return invalidChar_dyn(); }
		if (HX_FIELD_EQ(inName,"parseString") ) { return parseString_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"invalidNumber") ) { return invalidNumber_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Json_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"str") ) { str=inValue.Cast< ::String >(); return inValue; }
		if (HX_FIELD_EQ(inName,"pos") ) { pos=inValue.Cast< int >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Json_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("str"));
	outFields->push(HX_CSTRING("pos"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("parse"),
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsString,(int)offsetof(Json_obj,str),HX_CSTRING("str")},
	{hx::fsInt,(int)offsetof(Json_obj,pos),HX_CSTRING("pos")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("str"),
	HX_CSTRING("pos"),
	HX_CSTRING("doParse"),
	HX_CSTRING("invalidChar"),
	HX_CSTRING("parseRec"),
	HX_CSTRING("parseString"),
	HX_CSTRING("invalidNumber"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Json_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Json_obj::__mClass,"__mClass");
};

#endif

Class Json_obj::__mClass;

void Json_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("haxe.Json"), hx::TCanCast< Json_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void Json_obj::__boot()
{
}

} // end namespace haxe
