#include <hxcpp.h>

#ifndef INCLUDED_flixel_system_input_keyboard_FlxKey
#include <flixel/system/input/keyboard/FlxKey.h>
#endif
namespace flixel{
namespace system{
namespace input{
namespace keyboard{

Void FlxKey_obj::__construct(::String Name)
{
HX_STACK_FRAME("flixel.system.input.keyboard.FlxKey","new",0x2eb7eff5,"flixel.system.input.keyboard.FlxKey.new","flixel/system/input/keyboard/FlxKey.hx",6,0x59e8635b)

HX_STACK_ARG(Name,"Name")
{
	HX_STACK_LINE(24)
	this->last = (int)0;
	HX_STACK_LINE(20)
	this->current = (int)0;
	HX_STACK_LINE(28)
	this->name = Name;
}
;
	return null();
}

FlxKey_obj::~FlxKey_obj() { }

Dynamic FlxKey_obj::__CreateEmpty() { return  new FlxKey_obj; }
hx::ObjectPtr< FlxKey_obj > FlxKey_obj::__new(::String Name)
{  hx::ObjectPtr< FlxKey_obj > result = new FlxKey_obj();
	result->__construct(Name);
	return result;}

Dynamic FlxKey_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< FlxKey_obj > result = new FlxKey_obj();
	result->__construct(inArgs[0]);
	return result;}

int FlxKey_obj::JUST_RELEASED;

int FlxKey_obj::RELEASED;

int FlxKey_obj::PRESSED;

int FlxKey_obj::JUST_PRESSED;


FlxKey_obj::FlxKey_obj()
{
}

void FlxKey_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(FlxKey);
	HX_MARK_MEMBER_NAME(name,"name");
	HX_MARK_MEMBER_NAME(current,"current");
	HX_MARK_MEMBER_NAME(last,"last");
	HX_MARK_END_CLASS();
}

void FlxKey_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(name,"name");
	HX_VISIT_MEMBER_NAME(current,"current");
	HX_VISIT_MEMBER_NAME(last,"last");
}

Dynamic FlxKey_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"name") ) { return name; }
		if (HX_FIELD_EQ(inName,"last") ) { return last; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"current") ) { return current; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic FlxKey_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"name") ) { name=inValue.Cast< ::String >(); return inValue; }
		if (HX_FIELD_EQ(inName,"last") ) { last=inValue.Cast< int >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"current") ) { current=inValue.Cast< int >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void FlxKey_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("name"));
	outFields->push(HX_CSTRING("current"));
	outFields->push(HX_CSTRING("last"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("JUST_RELEASED"),
	HX_CSTRING("RELEASED"),
	HX_CSTRING("PRESSED"),
	HX_CSTRING("JUST_PRESSED"),
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsString,(int)offsetof(FlxKey_obj,name),HX_CSTRING("name")},
	{hx::fsInt,(int)offsetof(FlxKey_obj,current),HX_CSTRING("current")},
	{hx::fsInt,(int)offsetof(FlxKey_obj,last),HX_CSTRING("last")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("name"),
	HX_CSTRING("current"),
	HX_CSTRING("last"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(FlxKey_obj::__mClass,"__mClass");
	HX_MARK_MEMBER_NAME(FlxKey_obj::JUST_RELEASED,"JUST_RELEASED");
	HX_MARK_MEMBER_NAME(FlxKey_obj::RELEASED,"RELEASED");
	HX_MARK_MEMBER_NAME(FlxKey_obj::PRESSED,"PRESSED");
	HX_MARK_MEMBER_NAME(FlxKey_obj::JUST_PRESSED,"JUST_PRESSED");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(FlxKey_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(FlxKey_obj::JUST_RELEASED,"JUST_RELEASED");
	HX_VISIT_MEMBER_NAME(FlxKey_obj::RELEASED,"RELEASED");
	HX_VISIT_MEMBER_NAME(FlxKey_obj::PRESSED,"PRESSED");
	HX_VISIT_MEMBER_NAME(FlxKey_obj::JUST_PRESSED,"JUST_PRESSED");
};

#endif

Class FlxKey_obj::__mClass;

void FlxKey_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("flixel.system.input.keyboard.FlxKey"), hx::TCanCast< FlxKey_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void FlxKey_obj::__boot()
{
	JUST_RELEASED= (int)-1;
	RELEASED= (int)0;
	PRESSED= (int)1;
	JUST_PRESSED= (int)2;
}

} // end namespace flixel
} // end namespace system
} // end namespace input
} // end namespace keyboard
