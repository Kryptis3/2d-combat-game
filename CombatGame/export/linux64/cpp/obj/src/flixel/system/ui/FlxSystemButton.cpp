#include <hxcpp.h>

#ifndef INCLUDED_Reflect
#include <Reflect.h>
#endif
#ifndef INCLUDED_flash_display_Bitmap
#include <flash/display/Bitmap.h>
#endif
#ifndef INCLUDED_flash_display_BitmapData
#include <flash/display/BitmapData.h>
#endif
#ifndef INCLUDED_flash_display_DisplayObject
#include <flash/display/DisplayObject.h>
#endif
#ifndef INCLUDED_flash_display_DisplayObjectContainer
#include <flash/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_flash_display_IBitmapDrawable
#include <flash/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_flash_display_InteractiveObject
#include <flash/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_flash_display_PixelSnapping
#include <flash/display/PixelSnapping.h>
#endif
#ifndef INCLUDED_flash_display_Sprite
#include <flash/display/Sprite.h>
#endif
#ifndef INCLUDED_flash_events_Event
#include <flash/events/Event.h>
#endif
#ifndef INCLUDED_flash_events_EventDispatcher
#include <flash/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_flash_events_IEventDispatcher
#include <flash/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_flash_events_MouseEvent
#include <flash/events/MouseEvent.h>
#endif
#ifndef INCLUDED_flixel_system_ui_FlxSystemButton
#include <flixel/system/ui/FlxSystemButton.h>
#endif
#ifndef INCLUDED_openfl_Assets
#include <openfl/Assets.h>
#endif
namespace flixel{
namespace system{
namespace ui{

Void FlxSystemButton_obj::__construct(::String IconPath,Dynamic DownHandler,hx::Null< bool >  __o_ToggleMode)
{
HX_STACK_FRAME("flixel.system.ui.FlxSystemButton","new",0x605e3518,"flixel.system.ui.FlxSystemButton.new","flixel/system/ui/FlxSystemButton.hx",13,0x167fa379)

HX_STACK_ARG(IconPath,"IconPath")

HX_STACK_ARG(DownHandler,"DownHandler")

HX_STACK_ARG(__o_ToggleMode,"ToggleMode")
bool ToggleMode = __o_ToggleMode.Default(false);
{
	HX_STACK_LINE(36)
	this->toggled = false;
	HX_STACK_LINE(32)
	this->toggleMode = false;
	HX_STACK_LINE(23)
	this->enabled = true;
	HX_STACK_LINE(63)
	super::__construct();
	HX_STACK_LINE(65)
	if (((IconPath != null()))){
		HX_STACK_LINE(67)
		this->icon = ::flash::display::Bitmap_obj::__new(::openfl::Assets_obj::getBitmapData(IconPath,false),null(),null());
		HX_STACK_LINE(68)
		this->addChild(this->icon);
	}
	HX_STACK_LINE(74)
	this->downHandler = DownHandler;
	HX_STACK_LINE(75)
	this->toggleMode = ToggleMode;
	HX_STACK_LINE(77)
	this->addEventListener(::flash::events::MouseEvent_obj::MOUSE_UP,this->onMouseUp_dyn(),null(),null(),null());
	HX_STACK_LINE(78)
	this->addEventListener(::flash::events::MouseEvent_obj::MOUSE_OUT,this->onMouseOut_dyn(),null(),null(),null());
	HX_STACK_LINE(79)
	this->addEventListener(::flash::events::MouseEvent_obj::MOUSE_OVER,this->onMouseOver_dyn(),null(),null(),null());
}
;
	return null();
}

FlxSystemButton_obj::~FlxSystemButton_obj() { }

Dynamic FlxSystemButton_obj::__CreateEmpty() { return  new FlxSystemButton_obj; }
hx::ObjectPtr< FlxSystemButton_obj > FlxSystemButton_obj::__new(::String IconPath,Dynamic DownHandler,hx::Null< bool >  __o_ToggleMode)
{  hx::ObjectPtr< FlxSystemButton_obj > result = new FlxSystemButton_obj();
	result->__construct(IconPath,DownHandler,__o_ToggleMode);
	return result;}

Dynamic FlxSystemButton_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< FlxSystemButton_obj > result = new FlxSystemButton_obj();
	result->__construct(inArgs[0],inArgs[1],inArgs[2]);
	return result;}

bool FlxSystemButton_obj::set_toggled( bool Value){
	HX_STACK_FRAME("flixel.system.ui.FlxSystemButton","set_toggled",0xc0fd262b,"flixel.system.ui.FlxSystemButton.set_toggled","flixel/system/ui/FlxSystemButton.hx",39,0x167fa379)
	HX_STACK_THIS(this)
	HX_STACK_ARG(Value,"Value")
	HX_STACK_LINE(40)
	if ((this->toggleMode)){
		HX_STACK_LINE(42)
		if ((Value)){
			HX_STACK_LINE(44)
			this->set_alpha(0.3);
		}
		else{
			HX_STACK_LINE(48)
			this->set_alpha((int)1);
		}
	}
	HX_STACK_LINE(51)
	return this->toggled = Value;
}


HX_DEFINE_DYNAMIC_FUNC1(FlxSystemButton_obj,set_toggled,return )

Void FlxSystemButton_obj::changeIcon( ::String IconPath){
{
		HX_STACK_FRAME("flixel.system.ui.FlxSystemButton","changeIcon",0x06083f51,"flixel.system.ui.FlxSystemButton.changeIcon","flixel/system/ui/FlxSystemButton.hx",88,0x167fa379)
		HX_STACK_THIS(this)
		HX_STACK_ARG(IconPath,"IconPath")
		HX_STACK_LINE(89)
		if (((this->icon != null()))){
			HX_STACK_LINE(91)
			this->removeChild(this->icon);
		}
		HX_STACK_LINE(94)
		this->icon = ::flash::display::Bitmap_obj::__new(::openfl::Assets_obj::getBitmapData(IconPath,false),null(),null());
		HX_STACK_LINE(95)
		this->addChild(this->icon);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(FlxSystemButton_obj,changeIcon,(void))

Void FlxSystemButton_obj::onMouseUp( ::flash::events::MouseEvent E){
{
		HX_STACK_FRAME("flixel.system.ui.FlxSystemButton","onMouseUp",0xe1786b39,"flixel.system.ui.FlxSystemButton.onMouseUp","flixel/system/ui/FlxSystemButton.hx",100,0x167fa379)
		HX_STACK_THIS(this)
		HX_STACK_ARG(E,"E")
		HX_STACK_LINE(100)
		if (((bool((this->downHandler != null())) && bool(this->enabled)))){
			HX_STACK_LINE(102)
			this->set_toggled(!(this->toggled));
			HX_STACK_LINE(103)
			::Reflect_obj::callMethod(null(),this->downHandler,Dynamic( Array_obj<Dynamic>::__new()));
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(FlxSystemButton_obj,onMouseUp,(void))

Void FlxSystemButton_obj::onMouseOver( ::flash::events::MouseEvent E){
{
		HX_STACK_FRAME("flixel.system.ui.FlxSystemButton","onMouseOver",0x7ce209b2,"flixel.system.ui.FlxSystemButton.onMouseOver","flixel/system/ui/FlxSystemButton.hx",109,0x167fa379)
		HX_STACK_THIS(this)
		HX_STACK_ARG(E,"E")
		HX_STACK_LINE(109)
		::flixel::system::ui::FlxSystemButton _g = hx::ObjectPtr<OBJ_>(this);		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(109)
		_g->set_alpha((_g->get_alpha() - 0.2));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(FlxSystemButton_obj,onMouseOver,(void))

Void FlxSystemButton_obj::onMouseOut( ::flash::events::MouseEvent E){
{
		HX_STACK_FRAME("flixel.system.ui.FlxSystemButton","onMouseOut",0x67e0ddf0,"flixel.system.ui.FlxSystemButton.onMouseOut","flixel/system/ui/FlxSystemButton.hx",114,0x167fa379)
		HX_STACK_THIS(this)
		HX_STACK_ARG(E,"E")
		HX_STACK_LINE(114)
		::flixel::system::ui::FlxSystemButton _g = hx::ObjectPtr<OBJ_>(this);		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(114)
		_g->set_alpha((_g->get_alpha() + 0.2));
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(FlxSystemButton_obj,onMouseOut,(void))

Void FlxSystemButton_obj::destroy( ){
{
		HX_STACK_FRAME("flixel.system.ui.FlxSystemButton","destroy",0x554d4db2,"flixel.system.ui.FlxSystemButton.destroy","flixel/system/ui/FlxSystemButton.hx",118,0x167fa379)
		HX_STACK_THIS(this)
		HX_STACK_LINE(119)
		this->removeEventListener(::flash::events::MouseEvent_obj::MOUSE_OUT,this->onMouseOut_dyn(),null());
		HX_STACK_LINE(120)
		this->removeEventListener(::flash::events::MouseEvent_obj::MOUSE_OVER,this->onMouseOver_dyn(),null());
		HX_STACK_LINE(121)
		this->removeEventListener(::flash::events::MouseEvent_obj::MOUSE_UP,this->onMouseUp_dyn(),null());
		HX_STACK_LINE(122)
		this->icon = null();
		HX_STACK_LINE(123)
		this->downHandler = null();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(FlxSystemButton_obj,destroy,(void))


FlxSystemButton_obj::FlxSystemButton_obj()
{
}

void FlxSystemButton_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(FlxSystemButton);
	HX_MARK_MEMBER_NAME(downHandler,"downHandler");
	HX_MARK_MEMBER_NAME(enabled,"enabled");
	HX_MARK_MEMBER_NAME(icon,"icon");
	HX_MARK_MEMBER_NAME(toggleMode,"toggleMode");
	HX_MARK_MEMBER_NAME(toggled,"toggled");
	super::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void FlxSystemButton_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(downHandler,"downHandler");
	HX_VISIT_MEMBER_NAME(enabled,"enabled");
	HX_VISIT_MEMBER_NAME(icon,"icon");
	HX_VISIT_MEMBER_NAME(toggleMode,"toggleMode");
	HX_VISIT_MEMBER_NAME(toggled,"toggled");
	super::__Visit(HX_VISIT_ARG);
}

Dynamic FlxSystemButton_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"icon") ) { return icon; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"enabled") ) { return enabled; }
		if (HX_FIELD_EQ(inName,"toggled") ) { return toggled; }
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"onMouseUp") ) { return onMouseUp_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"toggleMode") ) { return toggleMode; }
		if (HX_FIELD_EQ(inName,"changeIcon") ) { return changeIcon_dyn(); }
		if (HX_FIELD_EQ(inName,"onMouseOut") ) { return onMouseOut_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"downHandler") ) { return downHandler; }
		if (HX_FIELD_EQ(inName,"set_toggled") ) { return set_toggled_dyn(); }
		if (HX_FIELD_EQ(inName,"onMouseOver") ) { return onMouseOver_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic FlxSystemButton_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"icon") ) { icon=inValue.Cast< ::flash::display::Bitmap >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"enabled") ) { enabled=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"toggled") ) { if (inCallProp) return set_toggled(inValue);toggled=inValue.Cast< bool >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"toggleMode") ) { toggleMode=inValue.Cast< bool >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"downHandler") ) { downHandler=inValue.Cast< Dynamic >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void FlxSystemButton_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("downHandler"));
	outFields->push(HX_CSTRING("enabled"));
	outFields->push(HX_CSTRING("icon"));
	outFields->push(HX_CSTRING("toggleMode"));
	outFields->push(HX_CSTRING("toggled"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*Dynamic*/ ,(int)offsetof(FlxSystemButton_obj,downHandler),HX_CSTRING("downHandler")},
	{hx::fsBool,(int)offsetof(FlxSystemButton_obj,enabled),HX_CSTRING("enabled")},
	{hx::fsObject /*::flash::display::Bitmap*/ ,(int)offsetof(FlxSystemButton_obj,icon),HX_CSTRING("icon")},
	{hx::fsBool,(int)offsetof(FlxSystemButton_obj,toggleMode),HX_CSTRING("toggleMode")},
	{hx::fsBool,(int)offsetof(FlxSystemButton_obj,toggled),HX_CSTRING("toggled")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("downHandler"),
	HX_CSTRING("enabled"),
	HX_CSTRING("icon"),
	HX_CSTRING("toggleMode"),
	HX_CSTRING("toggled"),
	HX_CSTRING("set_toggled"),
	HX_CSTRING("changeIcon"),
	HX_CSTRING("onMouseUp"),
	HX_CSTRING("onMouseOver"),
	HX_CSTRING("onMouseOut"),
	HX_CSTRING("destroy"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(FlxSystemButton_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(FlxSystemButton_obj::__mClass,"__mClass");
};

#endif

Class FlxSystemButton_obj::__mClass;

void FlxSystemButton_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("flixel.system.ui.FlxSystemButton"), hx::TCanCast< FlxSystemButton_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void FlxSystemButton_obj::__boot()
{
}

} // end namespace flixel
} // end namespace system
} // end namespace ui
