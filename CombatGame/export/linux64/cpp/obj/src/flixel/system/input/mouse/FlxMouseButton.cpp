#include <hxcpp.h>

#ifndef INCLUDED_flash_events_Event
#include <flash/events/Event.h>
#endif
#ifndef INCLUDED_flash_events_MouseEvent
#include <flash/events/MouseEvent.h>
#endif
#ifndef INCLUDED_flixel_system_input_mouse_FlxMouseButton
#include <flixel/system/input/mouse/FlxMouseButton.h>
#endif
namespace flixel{
namespace system{
namespace input{
namespace mouse{

Void FlxMouseButton_obj::__construct(hx::Null< bool >  __o_IsLeftMouse)
{
HX_STACK_FRAME("flixel.system.input.mouse.FlxMouseButton","new",0x7a71d0d1,"flixel.system.input.mouse.FlxMouseButton.new","flixel/system/input/mouse/FlxMouseButton.hx",9,0x279b3c9d)

HX_STACK_ARG(__o_IsLeftMouse,"IsLeftMouse")
bool IsLeftMouse = __o_IsLeftMouse.Default(false);
{
	HX_STACK_LINE(29)
	this->_isLeftMouse = false;
	HX_STACK_LINE(24)
	this->last = (int)0;
	HX_STACK_LINE(20)
	this->current = (int)0;
	HX_STACK_LINE(33)
	this->_isLeftMouse = IsLeftMouse;
}
;
	return null();
}

FlxMouseButton_obj::~FlxMouseButton_obj() { }

Dynamic FlxMouseButton_obj::__CreateEmpty() { return  new FlxMouseButton_obj; }
hx::ObjectPtr< FlxMouseButton_obj > FlxMouseButton_obj::__new(hx::Null< bool >  __o_IsLeftMouse)
{  hx::ObjectPtr< FlxMouseButton_obj > result = new FlxMouseButton_obj();
	result->__construct(__o_IsLeftMouse);
	return result;}

Dynamic FlxMouseButton_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< FlxMouseButton_obj > result = new FlxMouseButton_obj();
	result->__construct(inArgs[0]);
	return result;}

Void FlxMouseButton_obj::update( ){
{
		HX_STACK_FRAME("flixel.system.input.mouse.FlxMouseButton","update",0xfe58a238,"flixel.system.input.mouse.FlxMouseButton.update","flixel/system/input/mouse/FlxMouseButton.hx",40,0x279b3c9d)
		HX_STACK_THIS(this)
		HX_STACK_LINE(41)
		if (((bool((this->last == (int)-1)) && bool((this->current == (int)-1))))){
			HX_STACK_LINE(43)
			this->current = (int)0;
		}
		else{
			HX_STACK_LINE(45)
			if (((bool((this->last == (int)2)) && bool((this->current == (int)2))))){
				HX_STACK_LINE(47)
				this->current = (int)1;
			}
			else{
				HX_STACK_LINE(49)
				if (((bool((this->last == (int)-2)) && bool((this->current == (int)-2))))){
					HX_STACK_LINE(51)
					this->current = (int)0;
				}
			}
		}
		HX_STACK_LINE(54)
		this->last = this->current;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(FlxMouseButton_obj,update,(void))

Void FlxMouseButton_obj::onDown( ::flash::events::MouseEvent FlashEvent){
{
		HX_STACK_FRAME("flixel.system.input.mouse.FlxMouseButton","onDown",0x5c4b1d50,"flixel.system.input.mouse.FlxMouseButton.onDown","flixel/system/input/mouse/FlxMouseButton.hx",101,0x279b3c9d)
		HX_STACK_THIS(this)
		HX_STACK_ARG(FlashEvent,"FlashEvent")
		HX_STACK_LINE(101)
		if (((this->current > (int)0))){
			HX_STACK_LINE(103)
			this->current = (int)1;
		}
		else{
			HX_STACK_LINE(107)
			this->current = (int)2;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(FlxMouseButton_obj,onDown,(void))

Void FlxMouseButton_obj::onUp( ::flash::events::MouseEvent FlashEvent){
{
		HX_STACK_FRAME("flixel.system.input.mouse.FlxMouseButton","onUp",0xa9d4d3c9,"flixel.system.input.mouse.FlxMouseButton.onUp","flixel/system/input/mouse/FlxMouseButton.hx",125,0x279b3c9d)
		HX_STACK_THIS(this)
		HX_STACK_ARG(FlashEvent,"FlashEvent")
		HX_STACK_LINE(125)
		if (((this->current == (int)2))){
			HX_STACK_LINE(127)
			this->current = (int)-2;
		}
		else{
			HX_STACK_LINE(129)
			if (((this->current > (int)0))){
				HX_STACK_LINE(131)
				this->current = (int)-1;
			}
			else{
				HX_STACK_LINE(135)
				this->current = (int)0;
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(FlxMouseButton_obj,onUp,(void))

Void FlxMouseButton_obj::reset( ){
{
		HX_STACK_FRAME("flixel.system.input.mouse.FlxMouseButton","reset",0xa4bc0880,"flixel.system.input.mouse.FlxMouseButton.reset","flixel/system/input/mouse/FlxMouseButton.hx",143,0x279b3c9d)
		HX_STACK_THIS(this)
		HX_STACK_LINE(144)
		this->current = (int)0;
		HX_STACK_LINE(145)
		this->last = (int)0;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(FlxMouseButton_obj,reset,(void))

bool FlxMouseButton_obj::pressed( ){
	HX_STACK_FRAME("flixel.system.input.mouse.FlxMouseButton","pressed",0x2b4c5f93,"flixel.system.input.mouse.FlxMouseButton.pressed","flixel/system/input/mouse/FlxMouseButton.hx",152,0x279b3c9d)
	HX_STACK_THIS(this)
	HX_STACK_LINE(152)
	return (this->current > (int)0);
}


HX_DEFINE_DYNAMIC_FUNC0(FlxMouseButton_obj,pressed,return )

bool FlxMouseButton_obj::justPressed( ){
	HX_STACK_FRAME("flixel.system.input.mouse.FlxMouseButton","justPressed",0xd7546747,"flixel.system.input.mouse.FlxMouseButton.justPressed","flixel/system/input/mouse/FlxMouseButton.hx",158,0x279b3c9d)
	HX_STACK_THIS(this)
	HX_STACK_LINE(158)
	return (bool((this->current == (int)2)) || bool((this->current == (int)-2)));
}


HX_DEFINE_DYNAMIC_FUNC0(FlxMouseButton_obj,justPressed,return )

bool FlxMouseButton_obj::justReleased( ){
	HX_STACK_FRAME("flixel.system.input.mouse.FlxMouseButton","justReleased",0x995c0478,"flixel.system.input.mouse.FlxMouseButton.justReleased","flixel/system/input/mouse/FlxMouseButton.hx",164,0x279b3c9d)
	HX_STACK_THIS(this)
	HX_STACK_LINE(164)
	return (bool((this->current == (int)-1)) || bool((this->current == (int)-2)));
}


HX_DEFINE_DYNAMIC_FUNC0(FlxMouseButton_obj,justReleased,return )

int FlxMouseButton_obj::FAST_PRESS_RELEASE;

int FlxMouseButton_obj::JUST_RELEASED;

int FlxMouseButton_obj::RELEASED;

int FlxMouseButton_obj::PRESSED;

int FlxMouseButton_obj::JUST_PRESSED;


FlxMouseButton_obj::FlxMouseButton_obj()
{
}

void FlxMouseButton_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(FlxMouseButton);
	HX_MARK_MEMBER_NAME(current,"current");
	HX_MARK_MEMBER_NAME(last,"last");
	HX_MARK_MEMBER_NAME(_isLeftMouse,"_isLeftMouse");
	HX_MARK_END_CLASS();
}

void FlxMouseButton_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(current,"current");
	HX_VISIT_MEMBER_NAME(last,"last");
	HX_VISIT_MEMBER_NAME(_isLeftMouse,"_isLeftMouse");
}

Dynamic FlxMouseButton_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"last") ) { return last; }
		if (HX_FIELD_EQ(inName,"onUp") ) { return onUp_dyn(); }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"reset") ) { return reset_dyn(); }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		if (HX_FIELD_EQ(inName,"onDown") ) { return onDown_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"current") ) { return current; }
		if (HX_FIELD_EQ(inName,"pressed") ) { return pressed_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"justPressed") ) { return justPressed_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"_isLeftMouse") ) { return _isLeftMouse; }
		if (HX_FIELD_EQ(inName,"justReleased") ) { return justReleased_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic FlxMouseButton_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"last") ) { last=inValue.Cast< int >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"current") ) { current=inValue.Cast< int >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"_isLeftMouse") ) { _isLeftMouse=inValue.Cast< bool >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void FlxMouseButton_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("current"));
	outFields->push(HX_CSTRING("last"));
	outFields->push(HX_CSTRING("_isLeftMouse"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("FAST_PRESS_RELEASE"),
	HX_CSTRING("JUST_RELEASED"),
	HX_CSTRING("RELEASED"),
	HX_CSTRING("PRESSED"),
	HX_CSTRING("JUST_PRESSED"),
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsInt,(int)offsetof(FlxMouseButton_obj,current),HX_CSTRING("current")},
	{hx::fsInt,(int)offsetof(FlxMouseButton_obj,last),HX_CSTRING("last")},
	{hx::fsBool,(int)offsetof(FlxMouseButton_obj,_isLeftMouse),HX_CSTRING("_isLeftMouse")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("current"),
	HX_CSTRING("last"),
	HX_CSTRING("_isLeftMouse"),
	HX_CSTRING("update"),
	HX_CSTRING("onDown"),
	HX_CSTRING("onUp"),
	HX_CSTRING("reset"),
	HX_CSTRING("pressed"),
	HX_CSTRING("justPressed"),
	HX_CSTRING("justReleased"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(FlxMouseButton_obj::__mClass,"__mClass");
	HX_MARK_MEMBER_NAME(FlxMouseButton_obj::FAST_PRESS_RELEASE,"FAST_PRESS_RELEASE");
	HX_MARK_MEMBER_NAME(FlxMouseButton_obj::JUST_RELEASED,"JUST_RELEASED");
	HX_MARK_MEMBER_NAME(FlxMouseButton_obj::RELEASED,"RELEASED");
	HX_MARK_MEMBER_NAME(FlxMouseButton_obj::PRESSED,"PRESSED");
	HX_MARK_MEMBER_NAME(FlxMouseButton_obj::JUST_PRESSED,"JUST_PRESSED");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(FlxMouseButton_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(FlxMouseButton_obj::FAST_PRESS_RELEASE,"FAST_PRESS_RELEASE");
	HX_VISIT_MEMBER_NAME(FlxMouseButton_obj::JUST_RELEASED,"JUST_RELEASED");
	HX_VISIT_MEMBER_NAME(FlxMouseButton_obj::RELEASED,"RELEASED");
	HX_VISIT_MEMBER_NAME(FlxMouseButton_obj::PRESSED,"PRESSED");
	HX_VISIT_MEMBER_NAME(FlxMouseButton_obj::JUST_PRESSED,"JUST_PRESSED");
};

#endif

Class FlxMouseButton_obj::__mClass;

void FlxMouseButton_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("flixel.system.input.mouse.FlxMouseButton"), hx::TCanCast< FlxMouseButton_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void FlxMouseButton_obj::__boot()
{
	FAST_PRESS_RELEASE= (int)-2;
	JUST_RELEASED= (int)-1;
	RELEASED= (int)0;
	PRESSED= (int)1;
	JUST_PRESSED= (int)2;
}

} // end namespace flixel
} // end namespace system
} // end namespace input
} // end namespace mouse
