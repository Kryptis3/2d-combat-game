#include <hxcpp.h>

#ifndef INCLUDED_IMap
#include <IMap.h>
#endif
#ifndef INCLUDED_Reflect
#include <Reflect.h>
#endif
#ifndef INCLUDED_Std
#include <Std.h>
#endif
#ifndef INCLUDED_flash_Lib
#include <flash/Lib.h>
#endif
#ifndef INCLUDED_flash_display_DisplayObject
#include <flash/display/DisplayObject.h>
#endif
#ifndef INCLUDED_flash_display_DisplayObjectContainer
#include <flash/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_flash_display_IBitmapDrawable
#include <flash/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_flash_display_InteractiveObject
#include <flash/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_flash_display_MovieClip
#include <flash/display/MovieClip.h>
#endif
#ifndef INCLUDED_flash_display_Sprite
#include <flash/display/Sprite.h>
#endif
#ifndef INCLUDED_flash_display_Stage
#include <flash/display/Stage.h>
#endif
#ifndef INCLUDED_flash_events_Event
#include <flash/events/Event.h>
#endif
#ifndef INCLUDED_flash_events_EventDispatcher
#include <flash/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_flash_events_IEventDispatcher
#include <flash/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_flash_events_KeyboardEvent
#include <flash/events/KeyboardEvent.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxGame
#include <flixel/FlxGame.h>
#endif
#ifndef INCLUDED_flixel_system_frontEnds_SoundFrontEnd
#include <flixel/system/frontEnds/SoundFrontEnd.h>
#endif
#ifndef INCLUDED_flixel_system_input_IFlxInput
#include <flixel/system/input/IFlxInput.h>
#endif
#ifndef INCLUDED_flixel_system_input_keyboard_FlxKey
#include <flixel/system/input/keyboard/FlxKey.h>
#endif
#ifndef INCLUDED_flixel_system_input_keyboard_FlxKeyboard
#include <flixel/system/input/keyboard/FlxKeyboard.h>
#endif
#ifndef INCLUDED_flixel_system_replay_CodeValuePair
#include <flixel/system/replay/CodeValuePair.h>
#endif
#ifndef INCLUDED_flixel_system_ui_FlxSoundTray
#include <flixel/system/ui/FlxSoundTray.h>
#endif
#ifndef INCLUDED_flixel_util_FlxArrayUtil
#include <flixel/util/FlxArrayUtil.h>
#endif
#ifndef INCLUDED_haxe_ds_StringMap
#include <haxe/ds/StringMap.h>
#endif
namespace flixel{
namespace system{
namespace input{
namespace keyboard{

Void FlxKeyboard_obj::__construct()
{
HX_STACK_FRAME("flixel.system.input.keyboard.FlxKeyboard","new",0x797fd8b5,"flixel.system.input.keyboard.FlxKeyboard.new","flixel/system/input/keyboard/FlxKeyboard.hx",15,0xe9556e1b)
{
	HX_STACK_LINE(20)
	this->enabled = true;
	HX_STACK_LINE(39)
	this->_keyLookup = ::haxe::ds::StringMap_obj::__new();
	HX_STACK_LINE(41)
	this->_keyList = Array_obj< ::Dynamic >::__new();
	HX_STACK_LINE(42)
	::flixel::util::FlxArrayUtil_obj::setLength_flixel_system_input_keyboard_FlxKey(this->_keyList,(int)256);
	HX_STACK_LINE(44)
	int i;		HX_STACK_VAR(i,"i");
	HX_STACK_LINE(47)
	i = (int)65;
	HX_STACK_LINE(49)
	while(((i <= (int)90))){
		HX_STACK_LINE(51)
		this->addKey(::String::fromCharCode(i),i);
		HX_STACK_LINE(52)
		(i)++;
	}
	HX_STACK_LINE(56)
	i = (int)48;
	HX_STACK_LINE(57)
	this->addKey(HX_CSTRING("ZERO"),(i)++);
	HX_STACK_LINE(58)
	this->addKey(HX_CSTRING("ONE"),(i)++);
	HX_STACK_LINE(59)
	this->addKey(HX_CSTRING("TWO"),(i)++);
	HX_STACK_LINE(60)
	this->addKey(HX_CSTRING("THREE"),(i)++);
	HX_STACK_LINE(61)
	this->addKey(HX_CSTRING("FOUR"),(i)++);
	HX_STACK_LINE(62)
	this->addKey(HX_CSTRING("FIVE"),(i)++);
	HX_STACK_LINE(63)
	this->addKey(HX_CSTRING("SIX"),(i)++);
	HX_STACK_LINE(64)
	this->addKey(HX_CSTRING("SEVEN"),(i)++);
	HX_STACK_LINE(65)
	this->addKey(HX_CSTRING("EIGHT"),(i)++);
	HX_STACK_LINE(66)
	this->addKey(HX_CSTRING("NINE"),(i)++);
	HX_STACK_LINE(80)
	this->addKey(HX_CSTRING("PAGEUP"),(int)33);
	HX_STACK_LINE(81)
	this->addKey(HX_CSTRING("PAGEDOWN"),(int)34);
	HX_STACK_LINE(82)
	this->addKey(HX_CSTRING("HOME"),(int)36);
	HX_STACK_LINE(83)
	this->addKey(HX_CSTRING("END"),(int)35);
	HX_STACK_LINE(84)
	this->addKey(HX_CSTRING("INSERT"),(int)45);
	HX_STACK_LINE(96)
	this->addKey(HX_CSTRING("ESCAPE"),(int)27);
	HX_STACK_LINE(97)
	this->addKey(HX_CSTRING("MINUS"),(int)189);
	HX_STACK_LINE(98)
	this->addKey(HX_CSTRING("PLUS"),(int)187);
	HX_STACK_LINE(99)
	this->addKey(HX_CSTRING("DELETE"),(int)46);
	HX_STACK_LINE(100)
	this->addKey(HX_CSTRING("BACKSPACE"),(int)8);
	HX_STACK_LINE(101)
	this->addKey(HX_CSTRING("LBRACKET"),(int)219);
	HX_STACK_LINE(102)
	this->addKey(HX_CSTRING("RBRACKET"),(int)221);
	HX_STACK_LINE(103)
	this->addKey(HX_CSTRING("BACKSLASH"),(int)220);
	HX_STACK_LINE(104)
	this->addKey(HX_CSTRING("CAPSLOCK"),(int)20);
	HX_STACK_LINE(105)
	this->addKey(HX_CSTRING("SEMICOLON"),(int)186);
	HX_STACK_LINE(106)
	this->addKey(HX_CSTRING("QUOTE"),(int)222);
	HX_STACK_LINE(107)
	this->addKey(HX_CSTRING("ENTER"),(int)13);
	HX_STACK_LINE(108)
	this->addKey(HX_CSTRING("SHIFT"),(int)16);
	HX_STACK_LINE(109)
	this->addKey(HX_CSTRING("COMMA"),(int)188);
	HX_STACK_LINE(110)
	this->addKey(HX_CSTRING("PERIOD"),(int)190);
	HX_STACK_LINE(111)
	this->addKey(HX_CSTRING("SLASH"),(int)191);
	HX_STACK_LINE(112)
	this->addKey(HX_CSTRING("NUMPADSLASH"),(int)191);
	HX_STACK_LINE(113)
	this->addKey(HX_CSTRING("GRAVEACCENT"),(int)192);
	HX_STACK_LINE(114)
	this->addKey(HX_CSTRING("CONTROL"),(int)17);
	HX_STACK_LINE(115)
	this->addKey(HX_CSTRING("ALT"),(int)18);
	HX_STACK_LINE(116)
	this->addKey(HX_CSTRING("SPACE"),(int)32);
	HX_STACK_LINE(117)
	this->addKey(HX_CSTRING("UP"),(int)38);
	HX_STACK_LINE(118)
	this->addKey(HX_CSTRING("DOWN"),(int)40);
	HX_STACK_LINE(119)
	this->addKey(HX_CSTRING("LEFT"),(int)37);
	HX_STACK_LINE(120)
	this->addKey(HX_CSTRING("RIGHT"),(int)39);
	HX_STACK_LINE(121)
	this->addKey(HX_CSTRING("TAB"),(int)9);
	HX_STACK_LINE(129)
	::flash::Lib_obj::get_current()->get_stage()->addEventListener(::flash::events::KeyboardEvent_obj::KEY_DOWN,this->onKeyDown_dyn(),null(),null(),null());
	HX_STACK_LINE(130)
	::flash::Lib_obj::get_current()->get_stage()->addEventListener(::flash::events::KeyboardEvent_obj::KEY_UP,this->onKeyUp_dyn(),null(),null(),null());
	HX_STACK_LINE(132)
	this->pressed = ::Reflect_obj::makeVarArgs(this->anyPressed_dyn());
	HX_STACK_LINE(133)
	this->justPressed = ::Reflect_obj::makeVarArgs(this->anyJustPressed_dyn());
	HX_STACK_LINE(134)
	this->justReleased = ::Reflect_obj::makeVarArgs(this->anyJustReleased_dyn());
}
;
	return null();
}

FlxKeyboard_obj::~FlxKeyboard_obj() { }

Dynamic FlxKeyboard_obj::__CreateEmpty() { return  new FlxKeyboard_obj; }
hx::ObjectPtr< FlxKeyboard_obj > FlxKeyboard_obj::__new()
{  hx::ObjectPtr< FlxKeyboard_obj > result = new FlxKeyboard_obj();
	result->__construct();
	return result;}

Dynamic FlxKeyboard_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< FlxKeyboard_obj > result = new FlxKeyboard_obj();
	result->__construct();
	return result;}

hx::Object *FlxKeyboard_obj::__ToInterface(const hx::type_info &inType) {
	if (inType==typeid( ::flixel::system::input::IFlxInput_obj)) return operator ::flixel::system::input::IFlxInput_obj *();
	return super::__ToInterface(inType);
}

Void FlxKeyboard_obj::addKey( ::String KeyName,int KeyCode){
{
		HX_STACK_FRAME("flixel.system.input.keyboard.FlxKeyboard","addKey",0xab3ab4c9,"flixel.system.input.keyboard.FlxKeyboard.addKey","flixel/system/input/keyboard/FlxKeyboard.hx",144,0xe9556e1b)
		HX_STACK_THIS(this)
		HX_STACK_ARG(KeyName,"KeyName")
		HX_STACK_ARG(KeyCode,"KeyCode")
		HX_STACK_LINE(145)
		this->_keyLookup->set(KeyName,KeyCode);
		HX_STACK_LINE(146)
		this->_keyList[KeyCode] = ::flixel::system::input::keyboard::FlxKey_obj::__new(KeyName);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(FlxKeyboard_obj,addKey,(void))

Void FlxKeyboard_obj::update( ){
{
		HX_STACK_FRAME("flixel.system.input.keyboard.FlxKeyboard","update",0x933da0d4,"flixel.system.input.keyboard.FlxKeyboard.update","flixel/system/input/keyboard/FlxKeyboard.hx",154,0xe9556e1b)
		HX_STACK_THIS(this)
		HX_STACK_LINE(154)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(154)
		Array< ::Dynamic > _g1 = this->_keyList;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(154)
		while(((_g < _g1->length))){
			HX_STACK_LINE(154)
			::flixel::system::input::keyboard::FlxKey key = _g1->__get(_g).StaticCast< ::flixel::system::input::keyboard::FlxKey >();		HX_STACK_VAR(key,"key");
			HX_STACK_LINE(154)
			++(_g);
			HX_STACK_LINE(156)
			if (((key == null()))){
				HX_STACK_LINE(158)
				continue;
			}
			HX_STACK_LINE(161)
			if (((bool((key->last == (int)-1)) && bool((key->current == (int)-1))))){
				HX_STACK_LINE(163)
				key->current = (int)0;
			}
			else{
				HX_STACK_LINE(165)
				if (((bool((key->last == (int)2)) && bool((key->current == (int)2))))){
					HX_STACK_LINE(167)
					key->current = (int)1;
				}
			}
			HX_STACK_LINE(170)
			key->last = key->current;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(FlxKeyboard_obj,update,(void))

Void FlxKeyboard_obj::reset( ){
{
		HX_STACK_FRAME("flixel.system.input.keyboard.FlxKeyboard","reset",0x0946d164,"flixel.system.input.keyboard.FlxKeyboard.reset","flixel/system/input/keyboard/FlxKeyboard.hx",179,0xe9556e1b)
		HX_STACK_THIS(this)
		HX_STACK_LINE(179)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(179)
		Array< ::Dynamic > _g1 = this->_keyList;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(179)
		while(((_g < _g1->length))){
			HX_STACK_LINE(179)
			::flixel::system::input::keyboard::FlxKey key = _g1->__get(_g).StaticCast< ::flixel::system::input::keyboard::FlxKey >();		HX_STACK_VAR(key,"key");
			HX_STACK_LINE(179)
			++(_g);
			HX_STACK_LINE(181)
			if (((key != null()))){
				HX_STACK_LINE(183)
				key->current = (int)0;
				HX_STACK_LINE(184)
				key->last = (int)0;
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(FlxKeyboard_obj,reset,(void))

bool FlxKeyboard_obj::anyPressed( Dynamic KeyArray){
	HX_STACK_FRAME("flixel.system.input.keyboard.FlxKeyboard","anyPressed",0x65a75161,"flixel.system.input.keyboard.FlxKeyboard.anyPressed","flixel/system/input/keyboard/FlxKeyboard.hx",203,0xe9556e1b)
	HX_STACK_THIS(this)
	HX_STACK_ARG(KeyArray,"KeyArray")
	HX_STACK_LINE(203)
	return this->checkKeyStatus(KeyArray,(int)1);
}


HX_DEFINE_DYNAMIC_FUNC1(FlxKeyboard_obj,anyPressed,return )

bool FlxKeyboard_obj::anyJustPressed( Dynamic KeyArray){
	HX_STACK_FRAME("flixel.system.input.keyboard.FlxKeyboard","anyJustPressed",0x75075815,"flixel.system.input.keyboard.FlxKeyboard.anyJustPressed","flixel/system/input/keyboard/FlxKeyboard.hx",220,0xe9556e1b)
	HX_STACK_THIS(this)
	HX_STACK_ARG(KeyArray,"KeyArray")
	HX_STACK_LINE(220)
	return this->checkKeyStatus(KeyArray,(int)2);
}


HX_DEFINE_DYNAMIC_FUNC1(FlxKeyboard_obj,anyJustPressed,return )

bool FlxKeyboard_obj::anyJustReleased( Dynamic KeyArray){
	HX_STACK_FRAME("flixel.system.input.keyboard.FlxKeyboard","anyJustReleased",0xf83bc7ea,"flixel.system.input.keyboard.FlxKeyboard.anyJustReleased","flixel/system/input/keyboard/FlxKeyboard.hx",237,0xe9556e1b)
	HX_STACK_THIS(this)
	HX_STACK_ARG(KeyArray,"KeyArray")
	HX_STACK_LINE(237)
	return this->checkKeyStatus(KeyArray,(int)-1);
}


HX_DEFINE_DYNAMIC_FUNC1(FlxKeyboard_obj,anyJustReleased,return )

bool FlxKeyboard_obj::checkKeyStatus( Dynamic KeyArray,int Status){
	HX_STACK_FRAME("flixel.system.input.keyboard.FlxKeyboard","checkKeyStatus",0xd30ab214,"flixel.system.input.keyboard.FlxKeyboard.checkKeyStatus","flixel/system/input/keyboard/FlxKeyboard.hx",247,0xe9556e1b)
	HX_STACK_THIS(this)
	HX_STACK_ARG(KeyArray,"KeyArray")
	HX_STACK_ARG(Status,"Status")
	HX_STACK_LINE(248)
	if (((KeyArray == null()))){
		HX_STACK_LINE(250)
		return false;
	}
	HX_STACK_LINE(253)
	{
		HX_STACK_LINE(253)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(253)
		while(((_g < KeyArray->__Field(HX_CSTRING("length"),true)))){
			HX_STACK_LINE(253)
			Dynamic key = KeyArray->__GetItem(_g);		HX_STACK_VAR(key,"key");
			HX_STACK_LINE(253)
			++(_g);
			HX_STACK_LINE(256)
			key = ::Std_obj::string(key).toUpperCase();
			struct _Function_3_1{
				inline static Dynamic Block( Dynamic &key,::flixel::system::input::keyboard::FlxKeyboard_obj *__this){
					HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","flixel/system/input/keyboard/FlxKeyboard.hx",258,0xe9556e1b)
					{
						HX_STACK_LINE(258)
						::String key1 = key;		HX_STACK_VAR(key1,"key1");
						HX_STACK_LINE(258)
						return __this->_keyLookup->get(key1);
					}
					return null();
				}
			};
			HX_STACK_LINE(258)
			::flixel::system::input::keyboard::FlxKey k = this->_keyList->__get(_Function_3_1::Block(key,this)).StaticCast< ::flixel::system::input::keyboard::FlxKey >();		HX_STACK_VAR(k,"k");
			HX_STACK_LINE(259)
			if (((k != null()))){
				HX_STACK_LINE(261)
				if (((k->current == Status))){
					HX_STACK_LINE(263)
					return true;
				}
				else{
					HX_STACK_LINE(265)
					if (((bool((Status == (int)1)) && bool((k->current == (int)2))))){
						HX_STACK_LINE(267)
						return true;
					}
					else{
						HX_STACK_LINE(269)
						if (((bool((Status == (int)0)) && bool((k->current == (int)-1))))){
							HX_STACK_LINE(271)
							return true;
						}
					}
				}
			}
		}
	}
	HX_STACK_LINE(282)
	return false;
}


HX_DEFINE_DYNAMIC_FUNC2(FlxKeyboard_obj,checkKeyStatus,return )

Array< ::Dynamic > FlxKeyboard_obj::record( ){
	HX_STACK_FRAME("flixel.system.input.keyboard.FlxKeyboard","record",0x0a24915c,"flixel.system.input.keyboard.FlxKeyboard.record","flixel/system/input/keyboard/FlxKeyboard.hx",293,0xe9556e1b)
	HX_STACK_THIS(this)
	HX_STACK_LINE(294)
	Array< ::Dynamic > data = null();		HX_STACK_VAR(data,"data");
	HX_STACK_LINE(295)
	int i = (int)0;		HX_STACK_VAR(i,"i");
	HX_STACK_LINE(297)
	while(((i < (int)256))){
		HX_STACK_LINE(299)
		::flixel::system::input::keyboard::FlxKey key = this->_keyList->__get((i)++).StaticCast< ::flixel::system::input::keyboard::FlxKey >();		HX_STACK_VAR(key,"key");
		HX_STACK_LINE(301)
		if (((bool((key == null())) || bool((key->current == (int)0))))){
			HX_STACK_LINE(303)
			continue;
		}
		HX_STACK_LINE(306)
		if (((data == null()))){
			HX_STACK_LINE(308)
			data = Array_obj< ::Dynamic >::__new();
		}
		HX_STACK_LINE(311)
		data->push(::flixel::system::replay::CodeValuePair_obj::__new((i - (int)1),key->current));
	}
	HX_STACK_LINE(313)
	return data;
}


HX_DEFINE_DYNAMIC_FUNC0(FlxKeyboard_obj,record,return )

Void FlxKeyboard_obj::playback( Array< ::Dynamic > Record){
{
		HX_STACK_FRAME("flixel.system.input.keyboard.FlxKeyboard","playback",0xe4d7b5a6,"flixel.system.input.keyboard.FlxKeyboard.playback","flixel/system/input/keyboard/FlxKeyboard.hx",323,0xe9556e1b)
		HX_STACK_THIS(this)
		HX_STACK_ARG(Record,"Record")
		HX_STACK_LINE(324)
		int i = (int)0;		HX_STACK_VAR(i,"i");
		HX_STACK_LINE(325)
		int l = Record->length;		HX_STACK_VAR(l,"l");
		HX_STACK_LINE(326)
		::flixel::system::replay::CodeValuePair o;		HX_STACK_VAR(o,"o");
		HX_STACK_LINE(327)
		::flixel::system::input::keyboard::FlxKey o2;		HX_STACK_VAR(o2,"o2");
		HX_STACK_LINE(329)
		while(((i < l))){
			HX_STACK_LINE(331)
			o = Record->__get((i)++).StaticCast< ::flixel::system::replay::CodeValuePair >();
			HX_STACK_LINE(332)
			o2 = this->_keyList->__get(o->code).StaticCast< ::flixel::system::input::keyboard::FlxKey >();
			HX_STACK_LINE(333)
			o2->current = o->value;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(FlxKeyboard_obj,playback,(void))

int FlxKeyboard_obj::getKeyCode( ::String KeyName){
	HX_STACK_FRAME("flixel.system.input.keyboard.FlxKeyboard","getKeyCode",0xc0a484c1,"flixel.system.input.keyboard.FlxKeyboard.getKeyCode","flixel/system/input/keyboard/FlxKeyboard.hx",345,0xe9556e1b)
	HX_STACK_THIS(this)
	HX_STACK_ARG(KeyName,"KeyName")
	HX_STACK_LINE(345)
	return this->_keyLookup->get(KeyName);
}


HX_DEFINE_DYNAMIC_FUNC1(FlxKeyboard_obj,getKeyCode,return )

Array< ::Dynamic > FlxKeyboard_obj::getIsDown( ){
	HX_STACK_FRAME("flixel.system.input.keyboard.FlxKeyboard","getIsDown",0xf8adbcb7,"flixel.system.input.keyboard.FlxKeyboard.getIsDown","flixel/system/input/keyboard/FlxKeyboard.hx",354,0xe9556e1b)
	HX_STACK_THIS(this)
	HX_STACK_LINE(355)
	Array< ::Dynamic > keysDown = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(keysDown,"keysDown");
	HX_STACK_LINE(357)
	{
		HX_STACK_LINE(357)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(357)
		Array< ::Dynamic > _g1 = this->_keyList;		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(357)
		while(((_g < _g1->length))){
			HX_STACK_LINE(357)
			::flixel::system::input::keyboard::FlxKey key = _g1->__get(_g).StaticCast< ::flixel::system::input::keyboard::FlxKey >();		HX_STACK_VAR(key,"key");
			HX_STACK_LINE(357)
			++(_g);
			HX_STACK_LINE(359)
			if (((bool((key != null())) && bool((key->current > (int)0))))){
				HX_STACK_LINE(361)
				keysDown->push(key);
			}
		}
	}
	HX_STACK_LINE(364)
	return keysDown;
}


HX_DEFINE_DYNAMIC_FUNC0(FlxKeyboard_obj,getIsDown,return )

Void FlxKeyboard_obj::destroy( ){
{
		HX_STACK_FRAME("flixel.system.input.keyboard.FlxKeyboard","destroy",0xc96583cf,"flixel.system.input.keyboard.FlxKeyboard.destroy","flixel/system/input/keyboard/FlxKeyboard.hx",371,0xe9556e1b)
		HX_STACK_THIS(this)
		HX_STACK_LINE(372)
		this->_keyList = null();
		HX_STACK_LINE(373)
		this->_keyLookup = null();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(FlxKeyboard_obj,destroy,(void))

Void FlxKeyboard_obj::onKeyUp( ::flash::events::KeyboardEvent FlashEvent){
{
		HX_STACK_FRAME("flixel.system.input.keyboard.FlxKeyboard","onKeyUp",0x1a1baf10,"flixel.system.input.keyboard.FlxKeyboard.onKeyUp","flixel/system/input/keyboard/FlxKeyboard.hx",382,0xe9556e1b)
		HX_STACK_THIS(this)
		HX_STACK_ARG(FlashEvent,"FlashEvent")
		HX_STACK_LINE(383)
		int c = FlashEvent->keyCode;		HX_STACK_VAR(c,"c");
		HX_STACK_LINE(394)
		if ((!(this->enabled))){
			HX_STACK_LINE(396)
			return null();
		}
		HX_STACK_LINE(402)
		if ((this->inKeyArray(::flixel::FlxG_obj::sound->muteKeys,c))){
			HX_STACK_LINE(404)
			::flixel::FlxG_obj::sound->muted = !(::flixel::FlxG_obj::sound->muted);
			HX_STACK_LINE(406)
			if (((::flixel::FlxG_obj::sound->volumeHandler != null()))){
				HX_STACK_LINE(408)
				::flixel::FlxG_obj::sound->volumeHandler((  ((::flixel::FlxG_obj::sound->muted)) ? Float((int)0) : Float(::flixel::FlxG_obj::sound->volume) ));
			}
			HX_STACK_LINE(412)
			if (((::flixel::FlxG_obj::game->soundTray != null()))){
				HX_STACK_LINE(414)
				::flixel::FlxG_obj::game->soundTray->show(null());
			}
		}
		else{
			HX_STACK_LINE(419)
			if ((this->inKeyArray(::flixel::FlxG_obj::sound->volumeDownKeys,c))){
				HX_STACK_LINE(421)
				::flixel::FlxG_obj::sound->muted = false;
				HX_STACK_LINE(422)
				{
					HX_STACK_LINE(422)
					::flixel::system::frontEnds::SoundFrontEnd _g = ::flixel::FlxG_obj::sound;		HX_STACK_VAR(_g,"_g");
					HX_STACK_LINE(422)
					_g->set_volume((_g->volume - 0.1));
				}
				HX_STACK_LINE(425)
				if (((::flixel::FlxG_obj::game->soundTray != null()))){
					HX_STACK_LINE(427)
					::flixel::FlxG_obj::game->soundTray->show(null());
				}
			}
			else{
				HX_STACK_LINE(432)
				if ((this->inKeyArray(::flixel::FlxG_obj::sound->volumeUpKeys,c))){
					HX_STACK_LINE(434)
					::flixel::FlxG_obj::sound->muted = false;
					HX_STACK_LINE(435)
					{
						HX_STACK_LINE(435)
						::flixel::system::frontEnds::SoundFrontEnd _g = ::flixel::FlxG_obj::sound;		HX_STACK_VAR(_g,"_g");
						HX_STACK_LINE(435)
						_g->set_volume((_g->volume + 0.1));
					}
					HX_STACK_LINE(438)
					if (((::flixel::FlxG_obj::game->soundTray != null()))){
						HX_STACK_LINE(440)
						::flixel::FlxG_obj::game->soundTray->show(null());
					}
				}
			}
		}
		HX_STACK_LINE(445)
		{
			HX_STACK_LINE(445)
			::flixel::system::input::keyboard::FlxKey obj = this->_keyList->__get(c).StaticCast< ::flixel::system::input::keyboard::FlxKey >();		HX_STACK_VAR(obj,"obj");
			HX_STACK_LINE(445)
			if (((obj != null()))){
				HX_STACK_LINE(445)
				if (((obj->current > (int)0))){
					HX_STACK_LINE(445)
					obj->current = (int)-1;
				}
				else{
					HX_STACK_LINE(445)
					obj->current = (int)0;
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(FlxKeyboard_obj,onKeyUp,(void))

Void FlxKeyboard_obj::onKeyDown( ::flash::events::KeyboardEvent FlashEvent){
{
		HX_STACK_FRAME("flixel.system.input.keyboard.FlxKeyboard","onKeyDown",0x90749857,"flixel.system.input.keyboard.FlxKeyboard.onKeyDown","flixel/system/input/keyboard/FlxKeyboard.hx",454,0xe9556e1b)
		HX_STACK_THIS(this)
		HX_STACK_ARG(FlashEvent,"FlashEvent")
		HX_STACK_LINE(455)
		int c = FlashEvent->keyCode;		HX_STACK_VAR(c,"c");
		HX_STACK_LINE(473)
		if ((this->enabled)){
			HX_STACK_LINE(475)
			::flixel::system::input::keyboard::FlxKey obj = this->_keyList->__get(c).StaticCast< ::flixel::system::input::keyboard::FlxKey >();		HX_STACK_VAR(obj,"obj");
			HX_STACK_LINE(475)
			if (((obj != null()))){
				HX_STACK_LINE(475)
				if (((obj->current > (int)0))){
					HX_STACK_LINE(475)
					obj->current = (int)1;
				}
				else{
					HX_STACK_LINE(475)
					obj->current = (int)2;
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(FlxKeyboard_obj,onKeyDown,(void))

bool FlxKeyboard_obj::inKeyArray( Array< ::String > KeyArray,int KeyCode){
	HX_STACK_FRAME("flixel.system.input.keyboard.FlxKeyboard","inKeyArray",0x9b95f4ca,"flixel.system.input.keyboard.FlxKeyboard.inKeyArray","flixel/system/input/keyboard/FlxKeyboard.hx",484,0xe9556e1b)
	HX_STACK_THIS(this)
	HX_STACK_ARG(KeyArray,"KeyArray")
	HX_STACK_ARG(KeyCode,"KeyCode")
	HX_STACK_LINE(485)
	if (((KeyArray == null()))){
		HX_STACK_LINE(487)
		return false;
	}
	else{
		HX_STACK_LINE(491)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(491)
		while(((_g < KeyArray->length))){
			HX_STACK_LINE(491)
			::String keyString = KeyArray->__get(_g);		HX_STACK_VAR(keyString,"keyString");
			HX_STACK_LINE(491)
			++(_g);
			HX_STACK_LINE(493)
			if (((  ((!(((keyString == HX_CSTRING("ANY")))))) ? bool((this->_keyLookup->get(keyString) == KeyCode)) : bool(true) ))){
				HX_STACK_LINE(495)
				return true;
			}
		}
	}
	HX_STACK_LINE(500)
	return false;
}


HX_DEFINE_DYNAMIC_FUNC2(FlxKeyboard_obj,inKeyArray,return )

Void FlxKeyboard_obj::updateKeyStates( int KeyCode,bool Down){
{
		HX_STACK_FRAME("flixel.system.input.keyboard.FlxKeyboard","updateKeyStates",0xa58fdccd,"flixel.system.input.keyboard.FlxKeyboard.updateKeyStates","flixel/system/input/keyboard/FlxKeyboard.hx",507,0xe9556e1b)
		HX_STACK_THIS(this)
		HX_STACK_ARG(KeyCode,"KeyCode")
		HX_STACK_ARG(Down,"Down")
		HX_STACK_LINE(508)
		::flixel::system::input::keyboard::FlxKey obj = this->_keyList->__get(KeyCode).StaticCast< ::flixel::system::input::keyboard::FlxKey >();		HX_STACK_VAR(obj,"obj");
		HX_STACK_LINE(510)
		if (((obj != null()))){
			HX_STACK_LINE(512)
			if (((obj->current > (int)0))){
				HX_STACK_LINE(514)
				if ((Down)){
					HX_STACK_LINE(516)
					obj->current = (int)1;
				}
				else{
					HX_STACK_LINE(520)
					obj->current = (int)-1;
				}
			}
			else{
				HX_STACK_LINE(525)
				if ((Down)){
					HX_STACK_LINE(527)
					obj->current = (int)2;
				}
				else{
					HX_STACK_LINE(531)
					obj->current = (int)0;
				}
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(FlxKeyboard_obj,updateKeyStates,(void))

Void FlxKeyboard_obj::onFocus( ){
{
		HX_STACK_FRAME("flixel.system.input.keyboard.FlxKeyboard","onFocus",0x3fa6550e,"flixel.system.input.keyboard.FlxKeyboard.onFocus","flixel/system/input/keyboard/FlxKeyboard.hx",537,0xe9556e1b)
		HX_STACK_THIS(this)
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(FlxKeyboard_obj,onFocus,(void))

Void FlxKeyboard_obj::onFocusLost( ){
{
		HX_STACK_FRAME("flixel.system.input.keyboard.FlxKeyboard","onFocusLost",0x5c21ca12,"flixel.system.input.keyboard.FlxKeyboard.onFocusLost","flixel/system/input/keyboard/FlxKeyboard.hx",541,0xe9556e1b)
		HX_STACK_THIS(this)
		HX_STACK_LINE(541)
		this->reset();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(FlxKeyboard_obj,onFocusLost,(void))

::String FlxKeyboard_obj::toString( ){
	HX_STACK_FRAME("flixel.system.input.keyboard.FlxKeyboard","toString",0xd6fb7437,"flixel.system.input.keyboard.FlxKeyboard.toString","flixel/system/input/keyboard/FlxKeyboard.hx",546,0xe9556e1b)
	HX_STACK_THIS(this)
	HX_STACK_LINE(546)
	return HX_CSTRING("FlxKeyboard");
}


HX_DEFINE_DYNAMIC_FUNC0(FlxKeyboard_obj,toString,return )

int FlxKeyboard_obj::TOTAL;


FlxKeyboard_obj::FlxKeyboard_obj()
{
}

void FlxKeyboard_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(FlxKeyboard);
	HX_MARK_MEMBER_NAME(enabled,"enabled");
	HX_MARK_MEMBER_NAME(_keyLookup,"_keyLookup");
	HX_MARK_MEMBER_NAME(_keyList,"_keyList");
	HX_MARK_MEMBER_NAME(pressed,"pressed");
	HX_MARK_MEMBER_NAME(justPressed,"justPressed");
	HX_MARK_MEMBER_NAME(justReleased,"justReleased");
	HX_MARK_END_CLASS();
}

void FlxKeyboard_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(enabled,"enabled");
	HX_VISIT_MEMBER_NAME(_keyLookup,"_keyLookup");
	HX_VISIT_MEMBER_NAME(_keyList,"_keyList");
	HX_VISIT_MEMBER_NAME(pressed,"pressed");
	HX_VISIT_MEMBER_NAME(justPressed,"justPressed");
	HX_VISIT_MEMBER_NAME(justReleased,"justReleased");
}

Dynamic FlxKeyboard_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"reset") ) { return reset_dyn(); }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"addKey") ) { return addKey_dyn(); }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		if (HX_FIELD_EQ(inName,"record") ) { return record_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"enabled") ) { return enabled; }
		if (HX_FIELD_EQ(inName,"pressed") ) { return pressed; }
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		if (HX_FIELD_EQ(inName,"onKeyUp") ) { return onKeyUp_dyn(); }
		if (HX_FIELD_EQ(inName,"onFocus") ) { return onFocus_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"_keyList") ) { return _keyList; }
		if (HX_FIELD_EQ(inName,"playback") ) { return playback_dyn(); }
		if (HX_FIELD_EQ(inName,"toString") ) { return toString_dyn(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"getIsDown") ) { return getIsDown_dyn(); }
		if (HX_FIELD_EQ(inName,"onKeyDown") ) { return onKeyDown_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"_keyLookup") ) { return _keyLookup; }
		if (HX_FIELD_EQ(inName,"anyPressed") ) { return anyPressed_dyn(); }
		if (HX_FIELD_EQ(inName,"getKeyCode") ) { return getKeyCode_dyn(); }
		if (HX_FIELD_EQ(inName,"inKeyArray") ) { return inKeyArray_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"justPressed") ) { return justPressed; }
		if (HX_FIELD_EQ(inName,"onFocusLost") ) { return onFocusLost_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"justReleased") ) { return justReleased; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"anyJustPressed") ) { return anyJustPressed_dyn(); }
		if (HX_FIELD_EQ(inName,"checkKeyStatus") ) { return checkKeyStatus_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"anyJustReleased") ) { return anyJustReleased_dyn(); }
		if (HX_FIELD_EQ(inName,"updateKeyStates") ) { return updateKeyStates_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic FlxKeyboard_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 7:
		if (HX_FIELD_EQ(inName,"enabled") ) { enabled=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"pressed") ) { pressed=inValue.Cast< Dynamic >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"_keyList") ) { _keyList=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"_keyLookup") ) { _keyLookup=inValue.Cast< ::haxe::ds::StringMap >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"justPressed") ) { justPressed=inValue.Cast< Dynamic >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"justReleased") ) { justReleased=inValue.Cast< Dynamic >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void FlxKeyboard_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("enabled"));
	outFields->push(HX_CSTRING("_keyLookup"));
	outFields->push(HX_CSTRING("_keyList"));
	outFields->push(HX_CSTRING("pressed"));
	outFields->push(HX_CSTRING("justPressed"));
	outFields->push(HX_CSTRING("justReleased"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("TOTAL"),
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsBool,(int)offsetof(FlxKeyboard_obj,enabled),HX_CSTRING("enabled")},
	{hx::fsObject /*::haxe::ds::StringMap*/ ,(int)offsetof(FlxKeyboard_obj,_keyLookup),HX_CSTRING("_keyLookup")},
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(FlxKeyboard_obj,_keyList),HX_CSTRING("_keyList")},
	{hx::fsObject /*Dynamic*/ ,(int)offsetof(FlxKeyboard_obj,pressed),HX_CSTRING("pressed")},
	{hx::fsObject /*Dynamic*/ ,(int)offsetof(FlxKeyboard_obj,justPressed),HX_CSTRING("justPressed")},
	{hx::fsObject /*Dynamic*/ ,(int)offsetof(FlxKeyboard_obj,justReleased),HX_CSTRING("justReleased")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("enabled"),
	HX_CSTRING("_keyLookup"),
	HX_CSTRING("_keyList"),
	HX_CSTRING("addKey"),
	HX_CSTRING("update"),
	HX_CSTRING("reset"),
	HX_CSTRING("pressed"),
	HX_CSTRING("anyPressed"),
	HX_CSTRING("justPressed"),
	HX_CSTRING("anyJustPressed"),
	HX_CSTRING("justReleased"),
	HX_CSTRING("anyJustReleased"),
	HX_CSTRING("checkKeyStatus"),
	HX_CSTRING("record"),
	HX_CSTRING("playback"),
	HX_CSTRING("getKeyCode"),
	HX_CSTRING("getIsDown"),
	HX_CSTRING("destroy"),
	HX_CSTRING("onKeyUp"),
	HX_CSTRING("onKeyDown"),
	HX_CSTRING("inKeyArray"),
	HX_CSTRING("updateKeyStates"),
	HX_CSTRING("onFocus"),
	HX_CSTRING("onFocusLost"),
	HX_CSTRING("toString"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(FlxKeyboard_obj::__mClass,"__mClass");
	HX_MARK_MEMBER_NAME(FlxKeyboard_obj::TOTAL,"TOTAL");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(FlxKeyboard_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(FlxKeyboard_obj::TOTAL,"TOTAL");
};

#endif

Class FlxKeyboard_obj::__mClass;

void FlxKeyboard_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("flixel.system.input.keyboard.FlxKeyboard"), hx::TCanCast< FlxKeyboard_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void FlxKeyboard_obj::__boot()
{
	TOTAL= (int)256;
}

} // end namespace flixel
} // end namespace system
} // end namespace input
} // end namespace keyboard
