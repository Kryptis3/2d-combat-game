#include <hxcpp.h>

#ifndef INCLUDED_Std
#include <Std.h>
#endif
#ifndef INCLUDED_Type
#include <Type.h>
#endif
#ifndef INCLUDED_flash_display_BitmapData
#include <flash/display/BitmapData.h>
#endif
#ifndef INCLUDED_flash_display_IBitmapDrawable
#include <flash/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_flash_geom_Rectangle
#include <flash/geom/Rectangle.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_system_frontEnds_BitmapFrontEnd
#include <flixel/system/frontEnds/BitmapFrontEnd.h>
#endif
#ifndef INCLUDED_flixel_system_layer_Region
#include <flixel/system/layer/Region.h>
#endif
#ifndef INCLUDED_flixel_system_layer_TileSheetData
#include <flixel/system/layer/TileSheetData.h>
#endif
#ifndef INCLUDED_flixel_system_layer_frames_FlxFrame
#include <flixel/system/layer/frames/FlxFrame.h>
#endif
#ifndef INCLUDED_flixel_util_loaders_CachedGraphics
#include <flixel/util/loaders/CachedGraphics.h>
#endif
#ifndef INCLUDED_flixel_util_loaders_TexturePackerData
#include <flixel/util/loaders/TexturePackerData.h>
#endif
#ifndef INCLUDED_flixel_util_loaders_TextureRegion
#include <flixel/util/loaders/TextureRegion.h>
#endif
#ifndef INCLUDED_openfl_Assets
#include <openfl/Assets.h>
#endif
namespace flixel{
namespace util{
namespace loaders{

Void CachedGraphics_obj::__construct(::String key,::flash::display::BitmapData bitmap,hx::Null< bool >  __o_persist)
{
HX_STACK_FRAME("flixel.util.loaders.CachedGraphics","new",0xc149abe7,"flixel.util.loaders.CachedGraphics.new","flixel/util/loaders/CachedGraphics.hx",9,0x7a61d3e8)

HX_STACK_ARG(key,"key")

HX_STACK_ARG(bitmap,"bitmap")

HX_STACK_ARG(__o_persist,"persist")
bool persist = __o_persist.Default(false);
{
	HX_STACK_LINE(61)
	this->destroyOnNoUse = false;
	HX_STACK_LINE(55)
	this->_useCount = (int)0;
	HX_STACK_LINE(41)
	this->isDumped = false;
	HX_STACK_LINE(28)
	this->persist = false;
	HX_STACK_LINE(65)
	this->key = key;
	HX_STACK_LINE(66)
	this->bitmap = bitmap;
	HX_STACK_LINE(67)
	this->persist = persist;
}
;
	return null();
}

CachedGraphics_obj::~CachedGraphics_obj() { }

Dynamic CachedGraphics_obj::__CreateEmpty() { return  new CachedGraphics_obj; }
hx::ObjectPtr< CachedGraphics_obj > CachedGraphics_obj::__new(::String key,::flash::display::BitmapData bitmap,hx::Null< bool >  __o_persist)
{  hx::ObjectPtr< CachedGraphics_obj > result = new CachedGraphics_obj();
	result->__construct(key,bitmap,__o_persist);
	return result;}

Dynamic CachedGraphics_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< CachedGraphics_obj > result = new CachedGraphics_obj();
	result->__construct(inArgs[0],inArgs[1],inArgs[2]);
	return result;}

Void CachedGraphics_obj::dump( ){
{
		HX_STACK_FRAME("flixel.util.loaders.CachedGraphics","dump",0x589cb7cd,"flixel.util.loaders.CachedGraphics.dump","flixel/util/loaders/CachedGraphics.hx",77,0x7a61d3e8)
		HX_STACK_THIS(this)
		HX_STACK_LINE(77)
		if ((this->get_canBeDumped())){
			HX_STACK_LINE(79)
			this->bitmap->dumpBits();
			HX_STACK_LINE(80)
			this->isDumped = true;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(CachedGraphics_obj,dump,(void))

Void CachedGraphics_obj::undump( ){
{
		HX_STACK_FRAME("flixel.util.loaders.CachedGraphics","undump",0x016856e6,"flixel.util.loaders.CachedGraphics.undump","flixel/util/loaders/CachedGraphics.hx",91,0x7a61d3e8)
		HX_STACK_THIS(this)
		HX_STACK_LINE(91)
		if ((this->isDumped)){
			HX_STACK_LINE(93)
			::flash::display::BitmapData newBitmap = this->getBitmapFromSystem();		HX_STACK_VAR(newBitmap,"newBitmap");
			HX_STACK_LINE(95)
			if (((newBitmap != null()))){
				HX_STACK_LINE(97)
				this->bitmap = newBitmap;
				HX_STACK_LINE(98)
				if (((this->_tilesheet != null()))){
					HX_STACK_LINE(101)
					this->_tilesheet->onContext(newBitmap);
				}
			}
			HX_STACK_LINE(105)
			this->isDumped = false;
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(CachedGraphics_obj,undump,(void))

Void CachedGraphics_obj::onContext( ){
{
		HX_STACK_FRAME("flixel.util.loaders.CachedGraphics","onContext",0x22400957,"flixel.util.loaders.CachedGraphics.onContext","flixel/util/loaders/CachedGraphics.hx",117,0x7a61d3e8)
		HX_STACK_THIS(this)
		HX_STACK_LINE(117)
		if ((this->isDumped)){
			HX_STACK_LINE(120)
			this->undump();
			HX_STACK_LINE(122)
			this->dump();
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(CachedGraphics_obj,onContext,(void))

::flixel::system::layer::TileSheetData CachedGraphics_obj::get_tilesheet( ){
	HX_STACK_FRAME("flixel.util.loaders.CachedGraphics","get_tilesheet",0xd5f1cecf,"flixel.util.loaders.CachedGraphics.get_tilesheet","flixel/util/loaders/CachedGraphics.hx",127,0x7a61d3e8)
	HX_STACK_THIS(this)
	HX_STACK_LINE(128)
	if (((this->_tilesheet == null()))){
		HX_STACK_LINE(130)
		if ((this->isDumped)){
			HX_STACK_LINE(131)
			this->onContext();
		}
		HX_STACK_LINE(133)
		this->_tilesheet = ::flixel::system::layer::TileSheetData_obj::__new(this->bitmap);
	}
	HX_STACK_LINE(136)
	return this->_tilesheet;
}


HX_DEFINE_DYNAMIC_FUNC0(CachedGraphics_obj,get_tilesheet,return )

::flash::display::BitmapData CachedGraphics_obj::getBitmapFromSystem( ){
	HX_STACK_FRAME("flixel.util.loaders.CachedGraphics","getBitmapFromSystem",0xd5289305,"flixel.util.loaders.CachedGraphics.getBitmapFromSystem","flixel/util/loaders/CachedGraphics.hx",140,0x7a61d3e8)
	HX_STACK_THIS(this)
	HX_STACK_LINE(141)
	::flash::display::BitmapData newBitmap = null();		HX_STACK_VAR(newBitmap,"newBitmap");
	HX_STACK_LINE(142)
	if (((this->assetsClass != null()))){
		HX_STACK_LINE(144)
		newBitmap = ::Type_obj::createInstance(hx::TCast< Class >::cast(this->assetsClass),Dynamic( Array_obj<Dynamic>::__new()));
	}
	else{
		HX_STACK_LINE(146)
		if (((this->assetsKey != null()))){
			HX_STACK_LINE(148)
			newBitmap = ::openfl::Assets_obj::getBitmapData(this->assetsKey,false);
		}
	}
	HX_STACK_LINE(151)
	return newBitmap;
}


HX_DEFINE_DYNAMIC_FUNC0(CachedGraphics_obj,getBitmapFromSystem,return )

::flixel::util::loaders::TextureRegion CachedGraphics_obj::getRegionForFrame( ::String frameName){
	HX_STACK_FRAME("flixel.util.loaders.CachedGraphics","getRegionForFrame",0x4be29a75,"flixel.util.loaders.CachedGraphics.getRegionForFrame","flixel/util/loaders/CachedGraphics.hx",155,0x7a61d3e8)
	HX_STACK_THIS(this)
	HX_STACK_ARG(frameName,"frameName")
	HX_STACK_LINE(156)
	::flixel::util::loaders::TextureRegion region = ::flixel::util::loaders::TextureRegion_obj::__new(hx::ObjectPtr<OBJ_>(this),null(),null(),null(),null(),null(),null(),null(),null());		HX_STACK_VAR(region,"region");
	HX_STACK_LINE(157)
	::flixel::system::layer::frames::FlxFrame frame = this->get_tilesheet()->getFrame(frameName);		HX_STACK_VAR(frame,"frame");
	HX_STACK_LINE(158)
	if (((frame != null()))){
		HX_STACK_LINE(160)
		region->region->startX = ::Std_obj::_int(frame->frame->x);
		HX_STACK_LINE(161)
		region->region->startY = ::Std_obj::_int(frame->frame->y);
		HX_STACK_LINE(162)
		region->region->width = ::Std_obj::_int(frame->frame->width);
		HX_STACK_LINE(163)
		region->region->height = ::Std_obj::_int(frame->frame->height);
	}
	HX_STACK_LINE(166)
	return region;
}


HX_DEFINE_DYNAMIC_FUNC1(CachedGraphics_obj,getRegionForFrame,return )

Void CachedGraphics_obj::destroy( ){
{
		HX_STACK_FRAME("flixel.util.loaders.CachedGraphics","destroy",0x801dd801,"flixel.util.loaders.CachedGraphics.destroy","flixel/util/loaders/CachedGraphics.hx",170,0x7a61d3e8)
		HX_STACK_THIS(this)
		HX_STACK_LINE(171)
		this->key = null();
		HX_STACK_LINE(172)
		if (((this->bitmap != null()))){
			HX_STACK_LINE(174)
			this->bitmap->dispose();
		}
		HX_STACK_LINE(176)
		this->bitmap = null();
		HX_STACK_LINE(177)
		if (((this->data != null()))){
			HX_STACK_LINE(179)
			this->data->destroy();
		}
		HX_STACK_LINE(181)
		this->data = null();
		HX_STACK_LINE(182)
		if (((this->_tilesheet != null()))){
			HX_STACK_LINE(184)
			this->_tilesheet->destroy();
		}
		HX_STACK_LINE(186)
		this->_tilesheet = null();
		HX_STACK_LINE(188)
		this->assetsKey = null();
		HX_STACK_LINE(189)
		this->assetsClass = null();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(CachedGraphics_obj,destroy,(void))

bool CachedGraphics_obj::get_canBeDumped( ){
	HX_STACK_FRAME("flixel.util.loaders.CachedGraphics","get_canBeDumped",0x64ad8a84,"flixel.util.loaders.CachedGraphics.get_canBeDumped","flixel/util/loaders/CachedGraphics.hx",194,0x7a61d3e8)
	HX_STACK_THIS(this)
	HX_STACK_LINE(194)
	return (bool((this->assetsClass != null())) || bool((this->assetsKey != null())));
}


HX_DEFINE_DYNAMIC_FUNC0(CachedGraphics_obj,get_canBeDumped,return )

int CachedGraphics_obj::get_useCount( ){
	HX_STACK_FRAME("flixel.util.loaders.CachedGraphics","get_useCount",0xdb2456aa,"flixel.util.loaders.CachedGraphics.get_useCount","flixel/util/loaders/CachedGraphics.hx",199,0x7a61d3e8)
	HX_STACK_THIS(this)
	HX_STACK_LINE(199)
	return this->_useCount;
}


HX_DEFINE_DYNAMIC_FUNC0(CachedGraphics_obj,get_useCount,return )

int CachedGraphics_obj::set_useCount( int Value){
	HX_STACK_FRAME("flixel.util.loaders.CachedGraphics","set_useCount",0xf01d7a1e,"flixel.util.loaders.CachedGraphics.set_useCount","flixel/util/loaders/CachedGraphics.hx",203,0x7a61d3e8)
	HX_STACK_THIS(this)
	HX_STACK_ARG(Value,"Value")
	HX_STACK_LINE(204)
	this->_useCount = Value;
	HX_STACK_LINE(206)
	if (((bool((bool((this->_useCount <= (int)0)) && bool(this->destroyOnNoUse))) && bool(!(this->persist))))){
		HX_STACK_LINE(208)
		::flixel::FlxG_obj::bitmap->remove(this->key);
	}
	HX_STACK_LINE(211)
	return Value;
}


HX_DEFINE_DYNAMIC_FUNC1(CachedGraphics_obj,set_useCount,return )


CachedGraphics_obj::CachedGraphics_obj()
{
}

void CachedGraphics_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(CachedGraphics);
	HX_MARK_MEMBER_NAME(key,"key");
	HX_MARK_MEMBER_NAME(bitmap,"bitmap");
	HX_MARK_MEMBER_NAME(data,"data");
	HX_MARK_MEMBER_NAME(_tilesheet,"_tilesheet");
	HX_MARK_MEMBER_NAME(persist,"persist");
	HX_MARK_MEMBER_NAME(assetsKey,"assetsKey");
	HX_MARK_MEMBER_NAME(assetsClass,"assetsClass");
	HX_MARK_MEMBER_NAME(isDumped,"isDumped");
	HX_MARK_MEMBER_NAME(canBeDumped,"canBeDumped");
	HX_MARK_MEMBER_NAME(tilesheet,"tilesheet");
	HX_MARK_MEMBER_NAME(_useCount,"_useCount");
	HX_MARK_MEMBER_NAME(destroyOnNoUse,"destroyOnNoUse");
	HX_MARK_END_CLASS();
}

void CachedGraphics_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(key,"key");
	HX_VISIT_MEMBER_NAME(bitmap,"bitmap");
	HX_VISIT_MEMBER_NAME(data,"data");
	HX_VISIT_MEMBER_NAME(_tilesheet,"_tilesheet");
	HX_VISIT_MEMBER_NAME(persist,"persist");
	HX_VISIT_MEMBER_NAME(assetsKey,"assetsKey");
	HX_VISIT_MEMBER_NAME(assetsClass,"assetsClass");
	HX_VISIT_MEMBER_NAME(isDumped,"isDumped");
	HX_VISIT_MEMBER_NAME(canBeDumped,"canBeDumped");
	HX_VISIT_MEMBER_NAME(tilesheet,"tilesheet");
	HX_VISIT_MEMBER_NAME(_useCount,"_useCount");
	HX_VISIT_MEMBER_NAME(destroyOnNoUse,"destroyOnNoUse");
}

Dynamic CachedGraphics_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"key") ) { return key; }
		break;
	case 4:
		if (HX_FIELD_EQ(inName,"data") ) { return data; }
		if (HX_FIELD_EQ(inName,"dump") ) { return dump_dyn(); }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"bitmap") ) { return bitmap; }
		if (HX_FIELD_EQ(inName,"undump") ) { return undump_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"persist") ) { return persist; }
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"isDumped") ) { return isDumped; }
		if (HX_FIELD_EQ(inName,"useCount") ) { return get_useCount(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"assetsKey") ) { return assetsKey; }
		if (HX_FIELD_EQ(inName,"tilesheet") ) { return inCallProp ? get_tilesheet() : tilesheet; }
		if (HX_FIELD_EQ(inName,"_useCount") ) { return _useCount; }
		if (HX_FIELD_EQ(inName,"onContext") ) { return onContext_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"_tilesheet") ) { return _tilesheet; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"assetsClass") ) { return assetsClass; }
		if (HX_FIELD_EQ(inName,"canBeDumped") ) { return inCallProp ? get_canBeDumped() : canBeDumped; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"get_useCount") ) { return get_useCount_dyn(); }
		if (HX_FIELD_EQ(inName,"set_useCount") ) { return set_useCount_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"get_tilesheet") ) { return get_tilesheet_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"destroyOnNoUse") ) { return destroyOnNoUse; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"get_canBeDumped") ) { return get_canBeDumped_dyn(); }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"getRegionForFrame") ) { return getRegionForFrame_dyn(); }
		break;
	case 19:
		if (HX_FIELD_EQ(inName,"getBitmapFromSystem") ) { return getBitmapFromSystem_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic CachedGraphics_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"key") ) { key=inValue.Cast< ::String >(); return inValue; }
		break;
	case 4:
		if (HX_FIELD_EQ(inName,"data") ) { data=inValue.Cast< ::flixel::util::loaders::TexturePackerData >(); return inValue; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"bitmap") ) { bitmap=inValue.Cast< ::flash::display::BitmapData >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"persist") ) { persist=inValue.Cast< bool >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"isDumped") ) { isDumped=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"useCount") ) { return set_useCount(inValue); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"assetsKey") ) { assetsKey=inValue.Cast< ::String >(); return inValue; }
		if (HX_FIELD_EQ(inName,"tilesheet") ) { tilesheet=inValue.Cast< ::flixel::system::layer::TileSheetData >(); return inValue; }
		if (HX_FIELD_EQ(inName,"_useCount") ) { _useCount=inValue.Cast< int >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"_tilesheet") ) { _tilesheet=inValue.Cast< ::flixel::system::layer::TileSheetData >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"assetsClass") ) { assetsClass=inValue.Cast< ::Class >(); return inValue; }
		if (HX_FIELD_EQ(inName,"canBeDumped") ) { canBeDumped=inValue.Cast< bool >(); return inValue; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"destroyOnNoUse") ) { destroyOnNoUse=inValue.Cast< bool >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void CachedGraphics_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("key"));
	outFields->push(HX_CSTRING("bitmap"));
	outFields->push(HX_CSTRING("data"));
	outFields->push(HX_CSTRING("_tilesheet"));
	outFields->push(HX_CSTRING("persist"));
	outFields->push(HX_CSTRING("assetsKey"));
	outFields->push(HX_CSTRING("assetsClass"));
	outFields->push(HX_CSTRING("isDumped"));
	outFields->push(HX_CSTRING("canBeDumped"));
	outFields->push(HX_CSTRING("tilesheet"));
	outFields->push(HX_CSTRING("useCount"));
	outFields->push(HX_CSTRING("_useCount"));
	outFields->push(HX_CSTRING("destroyOnNoUse"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsString,(int)offsetof(CachedGraphics_obj,key),HX_CSTRING("key")},
	{hx::fsObject /*::flash::display::BitmapData*/ ,(int)offsetof(CachedGraphics_obj,bitmap),HX_CSTRING("bitmap")},
	{hx::fsObject /*::flixel::util::loaders::TexturePackerData*/ ,(int)offsetof(CachedGraphics_obj,data),HX_CSTRING("data")},
	{hx::fsObject /*::flixel::system::layer::TileSheetData*/ ,(int)offsetof(CachedGraphics_obj,_tilesheet),HX_CSTRING("_tilesheet")},
	{hx::fsBool,(int)offsetof(CachedGraphics_obj,persist),HX_CSTRING("persist")},
	{hx::fsString,(int)offsetof(CachedGraphics_obj,assetsKey),HX_CSTRING("assetsKey")},
	{hx::fsObject /*::Class*/ ,(int)offsetof(CachedGraphics_obj,assetsClass),HX_CSTRING("assetsClass")},
	{hx::fsBool,(int)offsetof(CachedGraphics_obj,isDumped),HX_CSTRING("isDumped")},
	{hx::fsBool,(int)offsetof(CachedGraphics_obj,canBeDumped),HX_CSTRING("canBeDumped")},
	{hx::fsObject /*::flixel::system::layer::TileSheetData*/ ,(int)offsetof(CachedGraphics_obj,tilesheet),HX_CSTRING("tilesheet")},
	{hx::fsInt,(int)offsetof(CachedGraphics_obj,_useCount),HX_CSTRING("_useCount")},
	{hx::fsBool,(int)offsetof(CachedGraphics_obj,destroyOnNoUse),HX_CSTRING("destroyOnNoUse")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("key"),
	HX_CSTRING("bitmap"),
	HX_CSTRING("data"),
	HX_CSTRING("_tilesheet"),
	HX_CSTRING("persist"),
	HX_CSTRING("assetsKey"),
	HX_CSTRING("assetsClass"),
	HX_CSTRING("isDumped"),
	HX_CSTRING("canBeDumped"),
	HX_CSTRING("tilesheet"),
	HX_CSTRING("_useCount"),
	HX_CSTRING("destroyOnNoUse"),
	HX_CSTRING("dump"),
	HX_CSTRING("undump"),
	HX_CSTRING("onContext"),
	HX_CSTRING("get_tilesheet"),
	HX_CSTRING("getBitmapFromSystem"),
	HX_CSTRING("getRegionForFrame"),
	HX_CSTRING("destroy"),
	HX_CSTRING("get_canBeDumped"),
	HX_CSTRING("get_useCount"),
	HX_CSTRING("set_useCount"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(CachedGraphics_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(CachedGraphics_obj::__mClass,"__mClass");
};

#endif

Class CachedGraphics_obj::__mClass;

void CachedGraphics_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("flixel.util.loaders.CachedGraphics"), hx::TCanCast< CachedGraphics_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void CachedGraphics_obj::__boot()
{
}

} // end namespace flixel
} // end namespace util
} // end namespace loaders
