#include <hxcpp.h>

#ifndef INCLUDED_Std
#include <Std.h>
#endif
#ifndef INCLUDED_flixel_util_FlxArrayUtil
#include <flixel/util/FlxArrayUtil.h>
#endif
#ifndef INCLUDED_flixel_util_FlxColorUtil
#include <flixel/util/FlxColorUtil.h>
#endif
#ifndef INCLUDED_flixel_util_FlxMath
#include <flixel/util/FlxMath.h>
#endif
#ifndef INCLUDED_flixel_util_FlxRandom
#include <flixel/util/FlxRandom.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
namespace flixel{
namespace util{

Void FlxRandom_obj::__construct()
{
	return null();
}

FlxRandom_obj::~FlxRandom_obj() { }

Dynamic FlxRandom_obj::__CreateEmpty() { return  new FlxRandom_obj; }
hx::ObjectPtr< FlxRandom_obj > FlxRandom_obj::__new()
{  hx::ObjectPtr< FlxRandom_obj > result = new FlxRandom_obj();
	result->__construct();
	return result;}

Dynamic FlxRandom_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< FlxRandom_obj > result = new FlxRandom_obj();
	result->__construct();
	return result;}

Float FlxRandom_obj::globalSeed;

int FlxRandom_obj::intHelper;

int FlxRandom_obj::MAX_RANGE;

int FlxRandom_obj::_int( ){
	HX_STACK_FRAME("flixel.util.FlxRandom","int",0xa7c5f55c,"flixel.util.FlxRandom.int","flixel/util/FlxRandom.hx",31,0xd3ba8ba3)
	HX_STACK_LINE(32)
	int result = ::Std_obj::_int(::flixel::util::FlxRandom_obj::intHelper);		HX_STACK_VAR(result,"result");
	HX_STACK_LINE(34)
	(result)++;
	HX_STACK_LINE(35)
	hx::MultEq(result,(int)75);
	HX_STACK_LINE(36)
	hx::ModEq(result,(int)65537);
	HX_STACK_LINE(37)
	(result)--;
	HX_STACK_LINE(39)
	(::flixel::util::FlxRandom_obj::intHelper)++;
	HX_STACK_LINE(41)
	if (((::flixel::util::FlxRandom_obj::intHelper == (int)65536))){
		HX_STACK_LINE(43)
		::flixel::util::FlxRandom_obj::intHelper = (int)0;
	}
	HX_STACK_LINE(46)
	return result;
}


STATIC_HX_DEFINE_DYNAMIC_FUNC0(FlxRandom_obj,_int,return )

int FlxRandom_obj::intRanged( Dynamic Min,Dynamic Max,Array< int > Excludes){
	HX_STACK_FRAME("flixel.util.FlxRandom","intRanged",0x32083683,"flixel.util.FlxRandom.intRanged","flixel/util/FlxRandom.hx",62,0xd3ba8ba3)
	HX_STACK_ARG(Min,"Min")
	HX_STACK_ARG(Max,"Max")
	HX_STACK_ARG(Excludes,"Excludes")
	HX_STACK_LINE(63)
	if (((Min == null()))){
		HX_STACK_LINE(65)
		Min = (int)0;
	}
	HX_STACK_LINE(68)
	if (((Max == null()))){
		HX_STACK_LINE(70)
		Max = (int)16777215;
	}
	HX_STACK_LINE(73)
	if (((Min == Max))){
		HX_STACK_LINE(75)
		return ::Math_obj::floor(Min);
	}
	HX_STACK_LINE(78)
	if (((Excludes != null()))){
		HX_STACK_LINE(81)
		Excludes->sort(::flixel::util::FlxMath_obj::numericComparison_dyn());
		HX_STACK_LINE(83)
		int result;		HX_STACK_VAR(result,"result");
		HX_STACK_LINE(85)
		do{
			HX_STACK_LINE(86)
			if (((Min < Max))){
				HX_STACK_LINE(88)
				result = ::Math_obj::floor((Min + (::Math_obj::random() * (((Max + (int)1) - Min)))));
			}
			else{
				HX_STACK_LINE(92)
				result = ::Math_obj::floor((Max + (::Math_obj::random() * (((Min + (int)1) - Max)))));
			}
		}
while(((::flixel::util::FlxArrayUtil_obj::indexOf_Int(Excludes,result,null()) >= (int)0)));
		HX_STACK_LINE(97)
		return result;
	}
	else{
		HX_STACK_LINE(102)
		if (((Min < Max))){
			HX_STACK_LINE(104)
			return ::Math_obj::floor((Min + (::Math_obj::random() * (((Max + (int)1) - Min)))));
		}
		else{
			HX_STACK_LINE(108)
			return ::Math_obj::floor((Max + (::Math_obj::random() * (((Min + (int)1) - Max)))));
		}
	}
	HX_STACK_LINE(78)
	return (int)0;
}


STATIC_HX_DEFINE_DYNAMIC_FUNC3(FlxRandom_obj,intRanged,return )

Float FlxRandom_obj::_float( ){
	HX_STACK_FRAME("flixel.util.FlxRandom","float",0xe1a96349,"flixel.util.FlxRandom.float","flixel/util/FlxRandom.hx",119,0xd3ba8ba3)
	HX_STACK_LINE(120)
	::flixel::util::FlxRandom_obj::globalSeed = (Float(hx::Mod(((int)69621 * ::Std_obj::_int((::flixel::util::FlxRandom_obj::globalSeed * (int)2147483647))),(int)2147483647)) / Float((int)2147483647));
	HX_STACK_LINE(121)
	if (((::flixel::util::FlxRandom_obj::globalSeed <= (int)0))){
		HX_STACK_LINE(121)
		hx::AddEq(::flixel::util::FlxRandom_obj::globalSeed,(int)1);
	}
	HX_STACK_LINE(122)
	return ::flixel::util::FlxRandom_obj::globalSeed;
}


STATIC_HX_DEFINE_DYNAMIC_FUNC0(FlxRandom_obj,_float,return )

Float FlxRandom_obj::floatRanged( Dynamic min,Dynamic max){
	HX_STACK_FRAME("flixel.util.FlxRandom","floatRanged",0x9b1a5430,"flixel.util.FlxRandom.floatRanged","flixel/util/FlxRandom.hx",137,0xd3ba8ba3)
	HX_STACK_ARG(min,"min")
	HX_STACK_ARG(max,"max")
	HX_STACK_LINE(138)
	if (((min == null()))){
		HX_STACK_LINE(140)
		min = (int)0;
	}
	HX_STACK_LINE(143)
	if (((max == null()))){
		HX_STACK_LINE(145)
		max = (int)16777215;
	}
	HX_STACK_LINE(148)
	if (((min == max))){
		HX_STACK_LINE(150)
		return min;
	}
	else{
		HX_STACK_LINE(152)
		if (((min < max))){
			HX_STACK_LINE(154)
			return (min + (::Math_obj::random() * ((max - min))));
		}
		else{
			HX_STACK_LINE(158)
			return (max + (::Math_obj::random() * ((min - max))));
		}
	}
	HX_STACK_LINE(148)
	return 0.;
}


STATIC_HX_DEFINE_DYNAMIC_FUNC2(FlxRandom_obj,floatRanged,return )

Float FlxRandom_obj::srand( Float Seed){
	HX_STACK_FRAME("flixel.util.FlxRandom","srand",0x61cba145,"flixel.util.FlxRandom.srand","flixel/util/FlxRandom.hx",169,0xd3ba8ba3)
	HX_STACK_ARG(Seed,"Seed")
	HX_STACK_LINE(169)
	return (Float(hx::Mod(((int)69621 * ::Std_obj::_int((Seed * (int)2147483647))),(int)2147483647)) / Float((int)2147483647));
}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(FlxRandom_obj,srand,return )

bool FlxRandom_obj::chanceRoll( hx::Null< Float >  __o_Chance){
Float Chance = __o_Chance.Default(50);
	HX_STACK_FRAME("flixel.util.FlxRandom","chanceRoll",0xff9c5ec4,"flixel.util.FlxRandom.chanceRoll","flixel/util/FlxRandom.hx",181,0xd3ba8ba3)
	HX_STACK_ARG(Chance,"Chance")
{
		HX_STACK_LINE(181)
		if (((Chance <= (int)0))){
			HX_STACK_LINE(183)
			return false;
		}
		else{
			HX_STACK_LINE(185)
			if (((Chance >= (int)100))){
				HX_STACK_LINE(187)
				return true;
			}
			else{
				HX_STACK_LINE(191)
				if ((((::Math_obj::random() * (int)100) >= Chance))){
					HX_STACK_LINE(193)
					return false;
				}
				else{
					HX_STACK_LINE(197)
					return true;
				}
			}
		}
		HX_STACK_LINE(181)
		return false;
	}
}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(FlxRandom_obj,chanceRoll,return )

Float FlxRandom_obj::sign( hx::Null< Float >  __o_Chance){
Float Chance = __o_Chance.Default(50);
	HX_STACK_FRAME("flixel.util.FlxRandom","sign",0x2c090730,"flixel.util.FlxRandom.sign","flixel/util/FlxRandom.hx",210,0xd3ba8ba3)
	HX_STACK_ARG(Chance,"Chance")
{
		HX_STACK_LINE(210)
		if ((::flixel::util::FlxRandom_obj::chanceRoll(Chance))){
			HX_STACK_LINE(210)
			return (int)1;
		}
		else{
			HX_STACK_LINE(210)
			return (int)-1;
		}
		HX_STACK_LINE(210)
		return 0.;
	}
}


STATIC_HX_DEFINE_DYNAMIC_FUNC1(FlxRandom_obj,sign,return )

int FlxRandom_obj::color( hx::Null< int >  __o_min,hx::Null< int >  __o_max,hx::Null< int >  __o_alpha){
int min = __o_min.Default(0);
int max = __o_max.Default(255);
int alpha = __o_alpha.Default(255);
	HX_STACK_FRAME("flixel.util.FlxRandom","color",0x296f0f10,"flixel.util.FlxRandom.color","flixel/util/FlxRandom.hx",226,0xd3ba8ba3)
	HX_STACK_ARG(min,"min")
	HX_STACK_ARG(max,"max")
	HX_STACK_ARG(alpha,"alpha")
{
		HX_STACK_LINE(226)
		return ::flixel::util::FlxColorUtil_obj::getRandomColor(min,max,alpha);
	}
}


STATIC_HX_DEFINE_DYNAMIC_FUNC3(FlxRandom_obj,color,return )


FlxRandom_obj::FlxRandom_obj()
{
}

void FlxRandom_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(FlxRandom);
	HX_MARK_END_CLASS();
}

void FlxRandom_obj::__Visit(HX_VISIT_PARAMS)
{
}

Dynamic FlxRandom_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 3:
		if (HX_FIELD_EQ(inName,"int") ) { return _int_dyn(); }
		break;
	case 4:
		if (HX_FIELD_EQ(inName,"sign") ) { return sign_dyn(); }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"float") ) { return _float_dyn(); }
		if (HX_FIELD_EQ(inName,"srand") ) { return srand_dyn(); }
		if (HX_FIELD_EQ(inName,"color") ) { return color_dyn(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"intHelper") ) { return intHelper; }
		if (HX_FIELD_EQ(inName,"intRanged") ) { return intRanged_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"globalSeed") ) { return globalSeed; }
		if (HX_FIELD_EQ(inName,"chanceRoll") ) { return chanceRoll_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"floatRanged") ) { return floatRanged_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic FlxRandom_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 9:
		if (HX_FIELD_EQ(inName,"intHelper") ) { intHelper=inValue.Cast< int >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"globalSeed") ) { globalSeed=inValue.Cast< Float >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void FlxRandom_obj::__GetFields(Array< ::String> &outFields)
{
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("globalSeed"),
	HX_CSTRING("intHelper"),
	HX_CSTRING("MAX_RANGE"),
	HX_CSTRING("int"),
	HX_CSTRING("intRanged"),
	HX_CSTRING("float"),
	HX_CSTRING("floatRanged"),
	HX_CSTRING("srand"),
	HX_CSTRING("chanceRoll"),
	HX_CSTRING("sign"),
	HX_CSTRING("color"),
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo *sMemberStorageInfo = 0;
#endif

static ::String sMemberFields[] = {
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(FlxRandom_obj::__mClass,"__mClass");
	HX_MARK_MEMBER_NAME(FlxRandom_obj::globalSeed,"globalSeed");
	HX_MARK_MEMBER_NAME(FlxRandom_obj::intHelper,"intHelper");
	HX_MARK_MEMBER_NAME(FlxRandom_obj::MAX_RANGE,"MAX_RANGE");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(FlxRandom_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(FlxRandom_obj::globalSeed,"globalSeed");
	HX_VISIT_MEMBER_NAME(FlxRandom_obj::intHelper,"intHelper");
	HX_VISIT_MEMBER_NAME(FlxRandom_obj::MAX_RANGE,"MAX_RANGE");
};

#endif

Class FlxRandom_obj::__mClass;

void FlxRandom_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("flixel.util.FlxRandom"), hx::TCanCast< FlxRandom_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void FlxRandom_obj::__boot()
{
	intHelper= (int)0;
	MAX_RANGE= (int)16777215;
}

} // end namespace flixel
} // end namespace util
