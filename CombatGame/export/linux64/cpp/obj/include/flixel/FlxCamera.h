#ifndef INCLUDED_flixel_FlxCamera
#define INCLUDED_flixel_FlxCamera

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/FlxBasic.h>
HX_DECLARE_CLASS2(flash,display,DisplayObject)
HX_DECLARE_CLASS2(flash,display,DisplayObjectContainer)
HX_DECLARE_CLASS2(flash,display,Graphics)
HX_DECLARE_CLASS2(flash,display,IBitmapDrawable)
HX_DECLARE_CLASS2(flash,display,InteractiveObject)
HX_DECLARE_CLASS2(flash,display,Sprite)
HX_DECLARE_CLASS2(flash,events,EventDispatcher)
HX_DECLARE_CLASS2(flash,events,IEventDispatcher)
HX_DECLARE_CLASS2(flash,geom,Point)
HX_DECLARE_CLASS2(flash,geom,Rectangle)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxCamera)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,IDestroyable)
HX_DECLARE_CLASS3(flixel,system,layer,DrawStackItem)
HX_DECLARE_CLASS2(flixel,util,FlxPoint)
HX_DECLARE_CLASS2(flixel,util,FlxRect)
HX_DECLARE_CLASS3(flixel,util,loaders,CachedGraphics)
namespace flixel{


class HXCPP_CLASS_ATTRIBUTES  FlxCamera_obj : public ::flixel::FlxBasic_obj{
	public:
		typedef ::flixel::FlxBasic_obj super;
		typedef FlxCamera_obj OBJ_;
		FlxCamera_obj();
		Void __construct(int X,int Y,int Width,int Height,hx::Null< Float >  __o_Zoom);

	public:
		static hx::ObjectPtr< FlxCamera_obj > __new(int X,int Y,int Width,int Height,hx::Null< Float >  __o_Zoom);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~FlxCamera_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("FlxCamera"); }

		Float x;
		Float y;
		int width;
		int height;
		int style;
		::flixel::FlxObject target;
		::flash::geom::Point followLead;
		Float followLerp;
		::flixel::util::FlxRect deadzone;
		::flixel::util::FlxRect bounds;
		::flixel::util::FlxPoint scroll;
		int bgColor;
		::flixel::util::FlxPoint _point;
		::flash::display::Sprite _flashSprite;
		Float _flashOffsetX;
		Float _flashOffsetY;
		::flash::geom::Rectangle _flashRect;
		::flash::geom::Point _flashPoint;
		int _fxFlashColor;
		Float _fxFlashDuration;
		Dynamic _fxFlashComplete;
		Dynamic &_fxFlashComplete_dyn() { return _fxFlashComplete;}
		Float _fxFlashAlpha;
		int _fxFadeColor;
		::flixel::util::FlxPoint _lastTargetPosition;
		::flixel::util::FlxPoint _scrollTarget;
		Float _fxFadeDuration;
		Dynamic _fxFadeComplete;
		Dynamic &_fxFadeComplete_dyn() { return _fxFadeComplete;}
		Float _fxFadeAlpha;
		Float _fxShakeIntensity;
		Float _fxShakeDuration;
		Dynamic _fxShakeComplete;
		Dynamic &_fxShakeComplete_dyn() { return _fxShakeComplete;}
		::flixel::util::FlxPoint _fxShakeOffset;
		int _fxShakeDirection;
		::flash::display::Sprite _canvas;
		::flixel::system::layer::DrawStackItem _currentStackItem;
		::flixel::system::layer::DrawStackItem _headOfDrawStack;
		virtual ::flixel::system::layer::DrawStackItem getDrawStackItem( ::flixel::util::loaders::CachedGraphics ObjGraphics,bool ObjColored,int ObjBlending,hx::Null< bool >  ObjAntialiasing);
		Dynamic getDrawStackItem_dyn();

		virtual Void clearDrawStack( );
		Dynamic clearDrawStack_dyn();

		virtual Void render( );
		Dynamic render_dyn();

		bool _fxFadeIn;
		virtual Void destroy( );

		virtual Void update( );

		virtual Void follow( ::flixel::FlxObject Target,hx::Null< int >  Style,::flixel::util::FlxPoint Offset,hx::Null< Float >  Lerp);
		Dynamic follow_dyn();

		virtual Void followAdjust( hx::Null< Float >  LeadX,hx::Null< Float >  LeadY);
		Dynamic followAdjust_dyn();

		virtual Void focusOn( ::flixel::util::FlxPoint point);
		Dynamic focusOn_dyn();

		virtual Void setBounds( hx::Null< Float >  X,hx::Null< Float >  Y,hx::Null< Float >  Width,hx::Null< Float >  Height,hx::Null< bool >  UpdateWorld);
		Dynamic setBounds_dyn();

		virtual Void flash( hx::Null< int >  Color,hx::Null< Float >  Duration,Dynamic OnComplete,hx::Null< bool >  Force);
		Dynamic flash_dyn();

		virtual Void fade( hx::Null< int >  Color,hx::Null< Float >  Duration,hx::Null< bool >  FadeIn,Dynamic OnComplete,hx::Null< bool >  Force);
		Dynamic fade_dyn();

		virtual Void shake( hx::Null< Float >  Intensity,hx::Null< Float >  Duration,Dynamic OnComplete,hx::Null< bool >  Force,hx::Null< int >  Direction);
		Dynamic shake_dyn();

		virtual Void stopFX( );
		Dynamic stopFX_dyn();

		virtual ::flixel::FlxCamera copyFrom( ::flixel::FlxCamera Camera);
		Dynamic copyFrom_dyn();

		Float zoom;
		virtual Float set_zoom( Float Zoom);
		Dynamic set_zoom_dyn();

		Float alpha;
		virtual Float set_alpha( Float Alpha);
		Dynamic set_alpha_dyn();

		Float angle;
		virtual Float set_angle( Float Angle);
		Dynamic set_angle_dyn();

		int color;
		virtual int set_color( int Color);
		Dynamic set_color_dyn();

		bool antialiasing;
		virtual bool set_antialiasing( bool Antialiasing);
		Dynamic set_antialiasing_dyn();

		virtual ::flixel::util::FlxPoint getScale( );
		Dynamic getScale_dyn();

		virtual Void setScale( Float X,Float Y);
		Dynamic setScale_dyn();

		virtual ::flash::display::Sprite getContainerSprite( );
		Dynamic getContainerSprite_dyn();

		virtual Void fill( int Color,hx::Null< bool >  BlendAlpha,hx::Null< Float >  FxAlpha,::flash::display::Graphics graphics);
		Dynamic fill_dyn();

		virtual Void drawFX( );
		Dynamic drawFX_dyn();

		virtual int set_width( int Value);
		Dynamic set_width_dyn();

		virtual int set_height( int Value);
		Dynamic set_height_dyn();

		bool useBgAlphaBlending;
		virtual bool set_useBgAlphaBlending( bool Value);
		Dynamic set_useBgAlphaBlending_dyn();

		virtual Void setSize( int Width,int Height);
		Dynamic setSize_dyn();

		virtual Void setPosition( hx::Null< Float >  X,hx::Null< Float >  Y);
		Dynamic setPosition_dyn();

		static int STYLE_LOCKON;
		static int STYLE_PLATFORMER;
		static int STYLE_TOPDOWN;
		static int STYLE_TOPDOWN_TIGHT;
		static int STYLE_SCREEN_BY_SCREEN;
		static int STYLE_NO_DEAD_ZONE;
		static int SHAKE_BOTH_AXES;
		static int SHAKE_HORIZONTAL_ONLY;
		static int SHAKE_VERTICAL_ONLY;
		static Float defaultZoom;
		static ::flixel::system::layer::DrawStackItem _storageHead;
};

} // end namespace flixel

#endif /* INCLUDED_flixel_FlxCamera */ 
