#ifndef INCLUDED_flixel_FlxSprite
#define INCLUDED_flixel_FlxSprite

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/FlxObject.h>
HX_DECLARE_CLASS2(flash,display,BitmapData)
HX_DECLARE_CLASS2(flash,display,BlendMode)
HX_DECLARE_CLASS2(flash,display,IBitmapDrawable)
HX_DECLARE_CLASS2(flash,geom,ColorTransform)
HX_DECLARE_CLASS2(flash,geom,Matrix)
HX_DECLARE_CLASS2(flash,geom,Point)
HX_DECLARE_CLASS2(flash,geom,Rectangle)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxCamera)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS1(flixel,IDestroyable)
HX_DECLARE_CLASS2(flixel,animation,FlxAnimationController)
HX_DECLARE_CLASS4(flixel,system,layer,frames,FlxFrame)
HX_DECLARE_CLASS2(flixel,util,FlxPoint)
namespace flixel{


class HXCPP_CLASS_ATTRIBUTES  FlxSprite_obj : public ::flixel::FlxObject_obj{
	public:
		typedef ::flixel::FlxObject_obj super;
		typedef FlxSprite_obj OBJ_;
		FlxSprite_obj();
		Void __construct(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y,Dynamic SimpleGraphic);

	public:
		static hx::ObjectPtr< FlxSprite_obj > __new(hx::Null< Float >  __o_X,hx::Null< Float >  __o_Y,Dynamic SimpleGraphic);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		~FlxSprite_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("FlxSprite"); }

		::flixel::animation::FlxAnimationController animation;
		::flixel::system::layer::frames::FlxFrame frame;
		::flash::display::BitmapData framePixels;
		int frameWidth;
		int frameHeight;
		int frames;
		Float bakedRotation;
		Float alpha;
		int facing;
		int flipped;
		::flixel::util::FlxPoint origin;
		::flixel::util::FlxPoint offset;
		::flixel::util::FlxPoint scale;
		bool antialiasing;
		bool dirty;
		::flash::display::BlendMode _blend;
		int _blendInt;
		int color;
		bool isColored;
		Float _red;
		Float _green;
		Float _blue;
		int _facingMult;
		::flash::geom::Point _flashPoint;
		::flash::geom::Rectangle _flashRect;
		::flash::geom::Rectangle _flashRect2;
		::flash::geom::Point _flashPointZero;
		::flash::geom::ColorTransform _colorTransform;
		bool useColorTransform;
		::flash::geom::Matrix _matrix;
		Float _halfWidth;
		Float _halfHeight;
		Float _sinAngle;
		Float _cosAngle;
		bool _angleChanged;
		::flixel::util::FlxPoint _offset;
		::flixel::util::FlxPoint _origin;
		::flixel::util::FlxPoint _scale;
		virtual Void initVars( );

		virtual Void destroy( );

		virtual ::flixel::FlxSprite clone( ::flixel::FlxSprite NewSprite);
		Dynamic clone_dyn();

		virtual ::flixel::FlxSprite loadfromSprite( ::flixel::FlxSprite Sprite);
		Dynamic loadfromSprite_dyn();

		virtual ::flixel::FlxSprite loadGraphic( Dynamic Graphic,hx::Null< bool >  Animated,hx::Null< bool >  Reverse,hx::Null< int >  Width,hx::Null< int >  Height,hx::Null< bool >  Unique,::String Key);
		Dynamic loadGraphic_dyn();

		virtual ::flixel::FlxSprite loadRotatedGraphic( Dynamic Graphic,hx::Null< int >  Rotations,hx::Null< int >  Frame,hx::Null< bool >  AntiAliasing,hx::Null< bool >  AutoBuffer,::String Key);
		Dynamic loadRotatedGraphic_dyn();

		virtual ::flixel::FlxSprite makeGraphic( int Width,int Height,hx::Null< int >  Color,hx::Null< bool >  Unique,::String Key);
		Dynamic makeGraphic_dyn();

		virtual ::flixel::FlxSprite loadImageFromTexture( Dynamic Data,hx::Null< bool >  Reverse,hx::Null< bool >  Unique,::String FrameName);
		Dynamic loadImageFromTexture_dyn();

		virtual ::flixel::FlxSprite loadRotatedImageFromTexture( Dynamic Data,::String Image,hx::Null< int >  Rotations,hx::Null< bool >  AntiAliasing,hx::Null< bool >  AutoBuffer);
		Dynamic loadRotatedImageFromTexture_dyn();

		virtual Void resetSize( );
		Dynamic resetSize_dyn();

		virtual Void resetFrameSize( );
		Dynamic resetFrameSize_dyn();

		virtual Void resetSizeFromFrame( );
		Dynamic resetSizeFromFrame_dyn();

		virtual Void setOriginToCenter( );
		Dynamic setOriginToCenter_dyn();

		virtual Void setGraphicDimensions( hx::Null< int >  Width,hx::Null< int >  Height,hx::Null< bool >  UpdateHitbox);
		Dynamic setGraphicDimensions_dyn();

		virtual Void updateHitbox( );
		Dynamic updateHitbox_dyn();

		virtual Void resetHelpers( );
		Dynamic resetHelpers_dyn();

		virtual Void update( );

		virtual Void draw( );

		virtual Void stamp( ::flixel::FlxSprite Brush,hx::Null< int >  X,hx::Null< int >  Y);
		Dynamic stamp_dyn();

		virtual Void drawFrame( hx::Null< bool >  Force);
		Dynamic drawFrame_dyn();

		virtual Void centerOffsets( hx::Null< bool >  AdjustPosition);
		Dynamic centerOffsets_dyn();

		virtual Array< ::Dynamic > replaceColor( int Color,int NewColor,hx::Null< bool >  FetchPositions);
		Dynamic replaceColor_dyn();

		virtual Void setColorTransformation( hx::Null< Float >  redMultiplier,hx::Null< Float >  greenMultiplier,hx::Null< Float >  blueMultiplier,hx::Null< Float >  alphaMultiplier,hx::Null< Float >  redOffset,hx::Null< Float >  greenOffset,hx::Null< Float >  blueOffset,hx::Null< Float >  alphaOffset);
		Dynamic setColorTransformation_dyn();

		virtual Void updateColorTransform( );
		Dynamic updateColorTransform_dyn();

		virtual bool onScreen( ::flixel::FlxCamera Camera);

		virtual bool overlapsPoint( ::flixel::util::FlxPoint point,hx::Null< bool >  InScreenSpace,::flixel::FlxCamera Camera);

		virtual bool pixelsOverlapPoint( ::flixel::util::FlxPoint point,hx::Null< int >  Mask,::flixel::FlxCamera Camera);
		Dynamic pixelsOverlapPoint_dyn();

		virtual Void calcFrame( hx::Null< bool >  AreYouSure);
		Dynamic calcFrame_dyn();

		virtual Void updateFrameData( );
		Dynamic updateFrameData_dyn();

		virtual ::flash::display::BitmapData getFlxFrameBitmapData( );
		Dynamic getFlxFrameBitmapData_dyn();

		virtual Void resetFrameBitmapDatas( );
		Dynamic resetFrameBitmapDatas_dyn();

		virtual bool simpleRenderSprite( );
		Dynamic simpleRenderSprite_dyn();

		virtual ::flash::display::BitmapData get_pixels( );
		Dynamic get_pixels_dyn();

		virtual ::flash::display::BitmapData set_pixels( ::flash::display::BitmapData Pixels);
		Dynamic set_pixels_dyn();

		virtual ::flixel::system::layer::frames::FlxFrame set_frame( ::flixel::system::layer::frames::FlxFrame Value);
		Dynamic set_frame_dyn();

		virtual int set_facing( int Direction);
		Dynamic set_facing_dyn();

		virtual Float set_alpha( Float Alpha);
		Dynamic set_alpha_dyn();

		virtual int set_color( int Color);
		Dynamic set_color_dyn();

		virtual ::flash::geom::ColorTransform get_colorTransform( );
		Dynamic get_colorTransform_dyn();

		virtual Float set_angle( Float Value);

		virtual ::flixel::util::FlxPoint set_origin( ::flixel::util::FlxPoint Value);
		Dynamic set_origin_dyn();

		virtual ::flixel::util::FlxPoint set_offset( ::flixel::util::FlxPoint Value);
		Dynamic set_offset_dyn();

		virtual ::flixel::util::FlxPoint set_scale( ::flixel::util::FlxPoint Value);
		Dynamic set_scale_dyn();

		virtual ::flash::display::BlendMode get_blend( );
		Dynamic get_blend_dyn();

		virtual ::flash::display::BlendMode set_blend( ::flash::display::BlendMode Value);
		Dynamic set_blend_dyn();

};

} // end namespace flixel

#endif /* INCLUDED_flixel_FlxSprite */ 
